ECHO OFF
CLS

ECHO b - Build
ECHO s - Send to production web server - test folder.
ECHO bs - b+s
ECHO sp - Send to production web server.
ECHO bsp - b+sp
ECHO w - Start ionic web server.
ECHO w2 - Start ionic production mode web server.
ECHO.
SET /P M=Type an option then press enter: 


IF %M%==b GOTO cb
IF %M%==s GOTO cs
IF %M%==bs GOTO cbs
IF %M%==sp GOTO csp
IF %M%==bsp GOTO cbsp
IF %M%==t GOTO ct
IF %M%==c GOTO cc
IF %M%==w GOTO cw
IF %M%==w2 GOTO cw2
@GOTO :EOF


:cb
@ECHO ON
@ECHO Starting
@TIME /T
ionic build --prod --aot --minifyjs --minifycss --optimizejs
@TIME /T
@ECHO Done
PAUSE
@GOTO :EOF

:cs
@ECHO ON
@ECHO Starting
@TIME /T
XCOPY  "www\*.*" "K:\Web\wwwroot\Intranet\data-test\" /C /R /Y /E
@TIME /T
@ECHO Done
PAUSE
@GOTO :EOF

:cbs
@ECHO ON
@ECHO Starting
@TIME /T
ionic build --prod --aot --minifyjs --minifycss --optimizejs && XCOPY  "www\*.*" "K:\Web\wwwroot\Intranet\data-test\" /C /R /Y /E
@TIME /T
@ECHO Done
PAUSE
@GOTO :EOF

:csp
@ECHO ON
XCOPY  "www\*.*" "K:\Web\wwwroot\Intranet\data\" /C /R /Y /E
@ECHO Done
PAUSE
@GOTO :EOF

:cbsp
@ECHO ON
ionic build --prod --aot --minifyjs --minifycss --optimizejs && XCOPY  "www\*.*" "K:\Web\wwwroot\Intranet\data\" /C /R /Y /E
@ECHO Done
PAUSE
@GOTO :EOF

:cw
@ECHO ON
@ECHO Starting
ionic serve --no-open -c
@ECHO Done
PAUSE
@GOTO :EOF

:cw2
@ECHO ON
@ECHO Starting
npm run ionic:build --prod
@ECHO Done
PAUSE


