import { Injectable } from '@angular/core';
import { TableBuilderWebApiProvider } from '../table-builder-web-api/table-builder-web-api';
export type WebApiGetWhatNames =
	| 'table-groups'
	| 'table-group'
	| 'tables-in-group'
	| 'table-info'
	| 'field-info'
	| 'table-data'
	| 'data-row'
	| 'table-fields'
	| 'table-field-info'
	| 'table-workflow-steps'
	| 'table-workflow-step'
	| 'execute-data-view'
	| 'field-uniqueness-types'
	| 'field-required-types'
	| 'admin-config-record'
	| 'admin-config-records'
	| 'email-trigger-times'
	| 'check-email-trigger-subscription'
	| 'data-views'
	| 'data-view'
	| 'sql-code-lookup-list'
	| 'report-id-by-name'
	| 'table-rights'
	| 'table-group-rights'
	| 'table-group-id-by-name'
	| 'table-id-by-name'
	| 'user-table-rights'
	| 'user-table-group-rights';
export type WebApiUpdateWhatNames =
	| 'table'
	| 'table-group'
	| 'field'
	| 'data'
	| 'admin'
	| 'sql-report-subscription'
	| 'data-view';
export type WebApiUpdateActionNames = 'update' | 'insert' | 'delete' | 'swap-order';
export type EditScreenMode = 'Table Group' | 'Table' | 'Field' | 'Data' | 'Admin' | 'SQL' | 'Workflow Step';
export type ListScreenMode =
	| 'Table Groups'
	| 'Tables'
	| 'Table Data'
	| 'Fields'
	| 'Admin'
	| 'Execute SQL'
	| 'SQL'
	| 'Workflow';
export enum TableBuilderReturnKind {
	TableRecordKind,
	TableRecordsKind,
	FieldRecordKind,
	FieldRecordsKind,
	FieldTypesKind,
	DataTableKind,
	DataRowKind,
	AdminConfigRecordsKind,
	AdminConfigRecordKind,
	ErrorKind,
	IDValueKind,
	IDValuesKind,
	LookupValuesKind,
	LookupValueKind,
	ItemIDKind,
	WorkflowStepRecordsKind,
	WorkflowStepRecordKind,
	TableRightRecordsKind,
	TableRightRecordKind,
	SuccessKind
}

export interface APISuccess {
	kind: TableBuilderReturnKind.SuccessKind;
}

export interface APIError {
	kind: TableBuilderReturnKind.ErrorKind;
	WebAPIMessageType: string;
	WebAPIMessage: string;
}

export interface ItemID {
	kind: TableBuilderReturnKind.ItemIDKind;
	ID: number;
}

export interface LookupValues {
	kind: TableBuilderReturnKind.LookupValuesKind;
	Values: LookupValue[];
}

export interface LookupValue {
	kind: TableBuilderReturnKind.LookupValueKind;
	Label: string;
	Code: string;
	Selected: boolean;
}

export interface TableRightRecords {
	kind: TableBuilderReturnKind.TableRightRecordsKind;
	TableRightRecs: TableRightRecord[];
}

export interface TableRightRecord {
	kind: TableBuilderReturnKind.TableRightRecordKind;
	ID: string;
	Right: string;
	Username: string;
	UserFullname: string;

	CreatedBy: string;
	CreatedByID: string;
	CreatedDate: string;
}

export interface WorkflowStepRecords {
	kind: TableBuilderReturnKind.WorkflowStepRecordsKind;
	WorkflowStepRecs: WorkflowStepRecord[];
}

export interface WorkflowStepRecord {
	kind: TableBuilderReturnKind.WorkflowStepRecordKind;
	ID: string;
	Order: string;
	Name: string;
	Comment: string;
	UserIDs: string;
	UserNames: string;
	Fields: string;
	FieldIDs: string;

	CreatedBy: string;
	CreatedByID: string;
	CreatedDate: string;
	ModifiedBy: string;
	ModifiedByID: string;
	ModifiedDate: string;
}

export interface TableRecords {
	kind: TableBuilderReturnKind.TableRecordsKind;
	TableRecs: TableRecord[];
}

export interface TableRecord {
	kind: TableBuilderReturnKind.TableRecordKind;
	ID: string;
	Name: string;
	Comment: string;
	Trigger: string;
	SQLDataView: string;
	Pinned: boolean;
	PinnedYN: string;

	CreatedBy: string;
	CreatedByID: string;
	CreatedDate: string;
	ModifiedBy: string;
	ModifiedByID: string;
	ModifiedDate: string;
}

export interface FieldRecords {
	kind: TableBuilderReturnKind.FieldRecordsKind;
	Values: FieldRecord[];
}

export interface FieldRecord {
	kind: TableBuilderReturnKind.FieldRecordKind;
	ID: string;
	Name: string;
	Order: string;
	Type: string;
	TypeSystemName: string;
	TypeID: string;
	Tooltip: string;
	UniquenessID: string;
	RequiredTypeID: string;
	RegEx: string;
	RegExFailMessage: string;
	ChoiceList: string;
	InsertHiddenYN: string;
	InsertHidden: boolean;
	SQLCodeLookupID: string;
	CodeMultiSelection: string;
	// SQLMultiCodeLookupID: string;

	CreatedBy: string;
	CreatedByID: string;
	CreatedDate: string;
	ModifiedBy: string;
	ModifiedByID: string;
	ModifiedDate: string;
}
export interface IDValues {
	kind: TableBuilderReturnKind.IDValuesKind;
	Values: IDValue[];
}

export interface IDValue {
	kind: TableBuilderReturnKind.IDValueKind;
	ID: string;
	Value: string;
}
export interface FieldTypes {
	kind: TableBuilderReturnKind.FieldTypesKind;
	Values: { ID: string; Value: string; SystemName: string; SQLLookupID: string }[];
}
export interface DataTable {
	kind: TableBuilderReturnKind.DataTableKind;
	Values: any;
}
export interface DataRow {
	kind: TableBuilderReturnKind.DataRowKind;
	Values: any[];
}

export interface AdminConfigRecords {
	kind: TableBuilderReturnKind.AdminConfigRecordsKind;
	Values: AdminConfigRecord[];
}

export interface AdminConfigRecord {
	kind: TableBuilderReturnKind.AdminConfigRecordKind;
	ID: string;
	ParentID: string;
	Type: string;
	Value: string;
	DataLinkID: string;

	CreatedBy: string;
	CreatedByID: string;
	CreatedDate: string;
	ModifiedBy: string;
	ModifiedByID: string;
	ModifiedDate: string;
}

@Injectable()
export class TableBuilderProvider {
	constructor(public oTableBuilderWebApiProvider: TableBuilderWebApiProvider) { }
}
