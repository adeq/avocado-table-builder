import { Injectable } from '@angular/core'
import {
	TableBuilderReturnKind,
	APIError,
	APISuccess,
	TableRecord,
	TableRecords,
	FieldRecord,
	FieldRecords,
	WebApiGetWhatNames,
	DataTable,
	DataRow,
	IDValue,
	IDValues,
	AdminConfigRecords,
	AdminConfigRecord,
	WebApiUpdateActionNames,
	WebApiUpdateWhatNames,
	TableRightRecords,
	FieldTypes,
	ItemID,
	LookupValues,
} from '../table-builder/table-builder'
import { UserProvider } from '../user/user'
import { Http, Headers, RequestOptions } from '@angular/http'

@Injectable()
export class TableBuilderWebApiProvider {
	constructor(public oUserProvider: UserProvider, public oHttp: Http) {}
	/**
   Returns an array of records or an APIError.
   */
	async getData(
		What: WebApiGetWhatNames,
		CurrentPage: number,
		RecordsPerPage: number,
		ID: string,
		Search: string,
	): Promise<
		| TableRecords
		| TableRecord
		| FieldRecords
		| FieldRecord
		| DataTable
		| DataRow
		| FieldTypes
		| IDValue
		| IDValues
		| AdminConfigRecords
		| AdminConfigRecord
		| TableRightRecords
		| LookupValues
		| ItemID
		| APIError
	> {
		try {
			const oReturn = await this.oUserProvider.checkLogin()
			if (oReturn === false)
				return <APIError>{
					WebAPIMessageType: 'NotLoggedIn',
					WebAPIMessage: '',
					kind: TableBuilderReturnKind.ErrorKind,
				}
			if (this.oUserProvider.strAppURL) {
				const oHeaders = new Headers({ Authorization: 'Bearer ' + this.oUserProvider.strToken })
				const oRequestOptions = new RequestOptions({ headers: oHeaders })
				const postResponse = await this.oHttp
					.post(
						this.oUserProvider.strAppURL + this.oUserProvider.strAppURLSuffix + 'Post',
						{
							What: What,
							Page: CurrentPage,
							RecordsPerPage: RecordsPerPage,
							ID: ID,
							Search: Search,
						},
						oRequestOptions,
					)
					.toPromise()
				const oResponseJSON: any = postResponse.json()
				const oFirstItem: any = oResponseJSON[0]
				if (oFirstItem.WebAPIMessageType && oFirstItem.WebAPIMessage) {
					return <APIError>Object.assign({}, oFirstItem, {
						kind: TableBuilderReturnKind.ErrorKind,
					})
				} else {
					if (What == 'table-groups' || What == 'tables-in-group' || What == 'data-views') {
						oResponseJSON.map((oEachArrayRow) => {
							// add kind to each row since it is not returned from the server
							oEachArrayRow.kind = TableBuilderReturnKind.TableRecordKind
							return oEachArrayRow
						})
						let oRecords: TableRecords = <TableRecords>{}
						oRecords.TableRecs = <TableRecord[]>oResponseJSON
						oRecords.kind = TableBuilderReturnKind.TableRecordsKind
						return oRecords
					} else if (What == 'table-info' || What == 'table-group' || What == 'data-view') {
						if (oFirstItem && oFirstItem.WebAPIMessageType && oFirstItem.WebAPIMessage) {
							return <APIError>Object.assign({}, oFirstItem, {
								kind: TableBuilderReturnKind.ErrorKind,
							})
						} else {
							oFirstItem.kind = TableBuilderReturnKind.TableRecordKind
							return <TableRecord>oFirstItem
						}
					} else if (
						What == 'table-data' ||
						What == 'table-fields' ||
						What == 'execute-data-view' ||
						What == 'table-workflow-steps'
					) {
						let oRecords: DataTable = <DataTable>{}
						oRecords.Values = oResponseJSON
						oRecords.kind = TableBuilderReturnKind.DataTableKind

						if (What == 'table-fields')
							if (!oResponseJSON[0] || !oResponseJSON[0].system______id_number_for_field)
								oRecords.Values = new Array()
						if (What == 'table-data')
							if (!oResponseJSON[0] || !oResponseJSON[0].system______row_id_number_for_table_data)
								oRecords.Values = new Array()
						return oRecords
					} else if (What == 'field-info') {
						if (oFirstItem && oFirstItem.WebAPIMessageType && oFirstItem.WebAPIMessage) {
							return <APIError>Object.assign({}, oFirstItem, {
								kind: TableBuilderReturnKind.ErrorKind,
							})
						} else {
							oFirstItem.kind = TableBuilderReturnKind.FieldRecordKind
							return <FieldRecord>oFirstItem
						}
					} else if (What == 'table-field-info') {
						let oRecords: FieldRecords = <FieldRecords>{}
						oRecords.Values = oResponseJSON
						oRecords.kind = TableBuilderReturnKind.FieldRecordsKind
						return oRecords
					} else if (What == 'field-uniqueness-types' || What == 'field-required-types') {
						let oRecords: FieldTypes = <FieldTypes>{}
						oRecords.Values = oResponseJSON
						oRecords.kind = TableBuilderReturnKind.FieldTypesKind
						return oRecords
					} else if (What == 'table-rights' || What == 'table-group-rights') {
						let oRecords: TableRightRecords = <TableRightRecords>{}
						oRecords.TableRightRecs = oResponseJSON
						oRecords.kind = TableBuilderReturnKind.TableRightRecordsKind
						return oRecords
					} else if (What == 'admin-config-records') {
						let oRecords: AdminConfigRecords = <AdminConfigRecords>{}
						oRecords.Values = oResponseJSON
						oRecords.kind = TableBuilderReturnKind.AdminConfigRecordsKind
						return oRecords
					} else if (What == 'admin-config-record') {
						if (oFirstItem && oFirstItem.WebAPIMessageType && oFirstItem.WebAPIMessage) {
							return <APIError>Object.assign({}, oFirstItem, {
								kind: TableBuilderReturnKind.ErrorKind,
							})
						} else {
							oFirstItem.kind = TableBuilderReturnKind.AdminConfigRecordKind
							return <AdminConfigRecord>oFirstItem
						}
					} else if (What == 'data-row') {
						if (oFirstItem && oFirstItem.WebAPIMessageType && oFirstItem.WebAPIMessage) {
							return <APIError>Object.assign({}, oFirstItem, {
								kind: TableBuilderReturnKind.ErrorKind,
							})
						} else {
							const oDataRow: DataRow = <DataRow>{}
							oDataRow.kind = TableBuilderReturnKind.DataRowKind
							oDataRow.Values = oFirstItem
							return oDataRow
						}
					} else if (
						What == 'email-trigger-times' ||
						What == 'user-table-rights' ||
						What == 'user-table-group-rights'
					) {
						let oRecords: IDValues = <IDValues>{}
						oRecords.Values = oResponseJSON
						oRecords.kind = TableBuilderReturnKind.IDValuesKind
						return oRecords
					} else if (What == 'check-email-trigger-subscription') {
						if (oFirstItem && oFirstItem.WebAPIMessageType && oFirstItem.WebAPIMessage) {
							return <APIError>Object.assign({}, oFirstItem, {
								kind: TableBuilderReturnKind.ErrorKind,
							})
						} else {
							const oDataRow: IDValue = <IDValue>{}
							oDataRow.kind = TableBuilderReturnKind.IDValueKind
							oDataRow.ID = oFirstItem.ID
							oDataRow.Value = oFirstItem.Value
							return oDataRow
						}
					} else if (What == 'sql-code-lookup-list') {
						let oRecords: LookupValues = <LookupValues>{}
						oRecords.Values = oResponseJSON
						oRecords.kind = TableBuilderReturnKind.LookupValuesKind
						return oRecords
					} else if (
						What == 'table-id-by-name' ||
						What == 'table-group-id-by-name' ||
						What == 'report-id-by-name'
					) {
						if (oFirstItem && oFirstItem.WebAPIMessageType && oFirstItem.WebAPIMessage) {
							return <APIError>Object.assign({}, oFirstItem, {
								kind: TableBuilderReturnKind.ErrorKind,
							})
						} else {
							const oItemID: ItemID = <ItemID>{}
							oItemID.kind = TableBuilderReturnKind.ItemIDKind
							oItemID.ID = oFirstItem.ID
							return oItemID
						}
					} else {
						let oAPIError: APIError = {
							WebAPIMessageType: 'Error',
							WebAPIMessage: 'Wrong type given.',
							kind: TableBuilderReturnKind.ErrorKind,
						}
						return oAPIError
					}
				}
			} else {
				return null
			}
		} catch (e) {
			let oAPIError: APIError = {
				WebAPIMessageType: 'Error',
				WebAPIMessage: e.Message,
				kind: TableBuilderReturnKind.ErrorKind,
			}
			console.log('Error:', e.Message)
			return oAPIError
		}
	}

	async updateData(
		What: WebApiUpdateWhatNames,
		Action: WebApiUpdateActionNames,
		ID: string,
		Value: string,
		CreatedBy: string,
		CreatedByID: string,
	): Promise<APIError | APISuccess> {
		try {
			const oReturn = await this.oUserProvider.checkLogin()
			if (oReturn === false)
				return <APIError>{
					WebAPIMessageType: 'NotLoggedIn',
					WebAPIMessage: '',
					kind: TableBuilderReturnKind.ErrorKind,
				}
			if (this.oUserProvider.strAppURL) {
				const oHeaders = new Headers({ Authorization: 'Bearer ' + this.oUserProvider.strToken })
				const oRequestOptions = new RequestOptions({ headers: oHeaders })
				const postResponse = await this.oHttp
					.post(
						this.oUserProvider.strAppURL + this.oUserProvider.strAppURLSuffix + 'Post',
						{
							IsUpdate: 'True',
							What: What,
							Action: Action,
							ID: ID,
							Value: Value,
							CreatedBy: CreatedBy,
							CreatedByID: CreatedByID,
						},
						oRequestOptions,
					)
					.toPromise()
				const oResponseJSON: any = postResponse.json()
				let oFirstItem: any
				if (oResponseJSON) {
					oFirstItem = oResponseJSON[0]
				}
				if (oFirstItem && oFirstItem.WebAPIMessageType && oFirstItem.WebAPIMessage) {
					return Object.assign({}, oFirstItem, {
						kind: TableBuilderReturnKind.ErrorKind,
					})
				}
				return <APISuccess>{
					kind: TableBuilderReturnKind.SuccessKind,
				}
			} else {
				return null
			}
		} catch (e) {
			const oAPIError: APIError = {
				WebAPIMessageType: 'Error',
				WebAPIMessage: e.message,
				kind: TableBuilderReturnKind.ErrorKind,
			}
			console.log('Error:', e.Message)
			return oAPIError
		}
	}
}
