import { Injectable } from '@angular/core'

@Injectable()
export class AppSettings {
	public TestMode = false
	public Version = '0.0.7'
	public WebAPIAppURLSuffix = 'table-builder/TableBuilder/'
	public WebAPIAppTestURLSuffix = 'table-builder/TableBuilderTest/'
	public AuthyOneTouchStatement = 'Please use your phone or desktop Authy program to click Approve on the OneTouch screen.  We apologize for this inconvenience but this is an important measure to protect our data and network.'
	public IEStatementTitle = 'Internet Explorer Detected'
	public IEStatement = "This page will not work in Internet Explorer.  Please install <a href='https://www.google.com/chrome/'>Chrome</a> or <a href='https://www.mozilla.org/en-US/firefox/'>Firefox</a>."
}
