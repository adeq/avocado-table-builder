import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';
import { IntranetLinks } from '../../app/app.component';
import { AppSettings } from '../../providers/app-settings/app-settings';
import { Events } from 'ionic-angular';

@Injectable()
export class UserProvider {
	public oUserInfo = {
		FirstName: '',
		LastName: '',
		FullName: '',
		Email: '',
		EmployeeID: '',
		UserName: '',
		OfficeNumber: '',
		WorkPhone: '',
		PhysicalOfficeName: '',
		IsSupervisor: '',
		SupervisorLevel: '',
		SupervisorID: '',
		Supervisor: '',
		SupervisorEmail: '',
		OrgDivisionCode: '',
		OrgDivision: '',
		OrgSectionCode: '',
		OrgSection: '',
		OrgSectionTree: '',
		DivisionCode: '',
		DivisionCodePlusName: '',
		DivisionAbbr: '',
		DivisionName: '',
		DivisionChiefID: '',
		DivisionChief: '',
		Permissions: [ '' ],
		LicenseGroups: '',
		LicenseGroupsDefault: '',
		ServerTime: '',
		ProgrammerPermission: false,
		AnnualLeaveBal: '',
		SickLeaveBal: '',
		UnreadMessages: ''
	};

	public strAppURL: string;
	public strAppURLSuffix: string;
	public strToken: string;
	public strLastUsernameUsed: string;
	public strLastPhoneNumberUsed: string;
	public booShowLoginForm: boolean = false;

	constructor(
		public oStorage: Storage,
		public oHttp: Http,
		private oAppSettings: AppSettings,
		private oEvents: Events
	) {}

	private async init() {
		if (this.oAppSettings.TestMode) {
			this.strAppURLSuffix = this.oAppSettings.WebAPIAppTestURLSuffix;
		} else {
			this.strAppURLSuffix = this.oAppSettings.WebAPIAppURLSuffix;
		}

		const oResponseURL = await this.oHttp
			.get('https://www.adeq.state.ar.us/webapis/common/api/login/GetAppURL')
			.toPromise();
		this.strAppURL = oResponseURL.json();
		if (!this.strToken) this.strToken = await this.oStorage.get('id_token');
		if (this.strToken) {
			if (!this.strAppURL) this.strAppURL = await this.oStorage.get('appurl');
			if (!this.oUserInfo.UserName) this.oUserInfo.UserName = await this.oStorage.get('username');

			const oHeaders = new Headers({ Authorization: 'Bearer ' + this.strToken });
			const oRequestOptions = new RequestOptions({ headers: oHeaders });
			//this.strAppURL + this.strAppURLSuffix + 'GetUserInfo',
			const responseUserInfo = await this.oHttp
				.post(
					'https://www.adeq.state.ar.us/webapis/common/api/common/GetUserInfo',
					{
						UserName: this.oUserInfo.UserName
					},
					oRequestOptions
				)
				.toPromise();
			const responseUserInfoData = responseUserInfo.json();
			if (responseUserInfoData[0].Permissions) {
				this.oUserInfo.Permissions = (<string>responseUserInfoData[0].Permissions).split(',');
				if (this.oUserInfo.Permissions && this.oUserInfo.Permissions.length > 0) {
					this.oUserInfo.Permissions.forEach((strEachPermission: string) => {
						strEachPermission = strEachPermission.trim().toLowerCase();
						if (strEachPermission == 'ps_memberpriv') this.oUserInfo.ProgrammerPermission = true;
					});
				}
			}

			if (responseUserInfoData[0].FirstName) {
				this.oUserInfo.FirstName = <string>responseUserInfoData[0].FirstName;
			}
			if (responseUserInfoData[0].LastName) {
				this.oUserInfo.LastName = <string>responseUserInfoData[0].LastName;
			}
			if (responseUserInfoData[0].EmployeeID) {
				this.oUserInfo.EmployeeID = <string>responseUserInfoData[0].EmployeeID;
			}
			if (responseUserInfoData[0].Email) {
				this.oUserInfo.Email = <string>responseUserInfoData[0].Email;
			}
			if (responseUserInfoData[0].WorkPhone) {
				this.oUserInfo.WorkPhone = <string>responseUserInfoData[0].WorkPhone;
			}
			if (responseUserInfoData[0].AnnualLeaveBal) {
				this.oUserInfo.AnnualLeaveBal = <string>responseUserInfoData[0].AnnualLeaveBal;
			}
			if (responseUserInfoData[0].SickLeaveBal) {
				this.oUserInfo.SickLeaveBal = <string>responseUserInfoData[0].SickLeaveBal;
			}
			if (responseUserInfoData[0].FullName) {
				this.oUserInfo.FullName = <string>responseUserInfoData[0].FullName;
			}
			if (responseUserInfoData[0].OfficeNumber) {
				this.oUserInfo.OfficeNumber = <string>responseUserInfoData[0].OfficeNumber;
			}
			if (responseUserInfoData[0].WorkPhone) {
				this.oUserInfo.WorkPhone = <string>responseUserInfoData[0].WorkPhone;
			}
			if (responseUserInfoData[0].PhysicalOfficeName) {
				this.oUserInfo.PhysicalOfficeName = <string>responseUserInfoData[0].PhysicalOfficeName;
			}
			if (responseUserInfoData[0].IsSupervisor) {
				this.oUserInfo.IsSupervisor = <string>responseUserInfoData[0].IsSupervisor;
			}
			if (responseUserInfoData[0].SupervisorLevel) {
				this.oUserInfo.SupervisorLevel = <string>responseUserInfoData[0].SupervisorLevel;
			}
			if (responseUserInfoData[0].SupervisorID) {
				this.oUserInfo.SupervisorID = <string>responseUserInfoData[0].SupervisorID;
			}
			if (responseUserInfoData[0].Supervisor) {
				this.oUserInfo.Supervisor = <string>responseUserInfoData[0].Supervisor;
			}
			if (responseUserInfoData[0].SupervisorEmail) {
				this.oUserInfo.SupervisorEmail = <string>responseUserInfoData[0].SupervisorEmail;
			}
			if (responseUserInfoData[0].OrgDivisionCode) {
				this.oUserInfo.OrgDivisionCode = <string>responseUserInfoData[0].OrgDivisionCode;
			}
			if (responseUserInfoData[0].OrgDivision) {
				this.oUserInfo.OrgDivision = <string>responseUserInfoData[0].OrgDivision;
			}
			if (responseUserInfoData[0].OrgSectionCode) {
				this.oUserInfo.OrgSectionCode = <string>responseUserInfoData[0].OrgSectionCode;
			}
			if (responseUserInfoData[0].OrgSection) {
				this.oUserInfo.OrgSection = <string>responseUserInfoData[0].OrgSection;
			}
			if (responseUserInfoData[0].OrgSectionTree) {
				this.oUserInfo.OrgSectionTree = <string>responseUserInfoData[0].OrgSectionTree;
			}
			if (responseUserInfoData[0].DivisionCode) {
				this.oUserInfo.DivisionCode = <string>responseUserInfoData[0].DivisionCode;
			}
			if (responseUserInfoData[0].DivisionCodePlusName) {
				this.oUserInfo.DivisionCodePlusName = <string>responseUserInfoData[0].DivisionCodePlusName;
			}
			if (responseUserInfoData[0].DivisionAbbr) {
				this.oUserInfo.DivisionAbbr = <string>responseUserInfoData[0].DivisionAbbr;
			}
			if (responseUserInfoData[0].DivisionName) {
				this.oUserInfo.DivisionName = <string>responseUserInfoData[0].DivisionName;
			}
			if (responseUserInfoData[0].DivisionChiefID) {
				this.oUserInfo.DivisionChiefID = <string>responseUserInfoData[0].DivisionChiefID;
			}
			if (responseUserInfoData[0].DivisionChief) {
				this.oUserInfo.DivisionChief = <string>responseUserInfoData[0].DivisionChief;
			}
			if (responseUserInfoData[0].LicenseGroups) {
				this.oUserInfo.LicenseGroups = <string>responseUserInfoData[0].LicenseGroups;
			}
			if (responseUserInfoData[0].LicenseGroupsDefault) {
				this.oUserInfo.LicenseGroupsDefault = <string>responseUserInfoData[0].LicenseGroupsDefault;
			}
			if (responseUserInfoData[0].ServerTime) {
				this.oUserInfo.ServerTime = <string>responseUserInfoData[0].ServerTime;
			}
			if (responseUserInfoData[0].UnreadMessages) {
				this.oUserInfo.UnreadMessages = <string>responseUserInfoData[0].UnreadMessages;
			}
		}
	}

	async getIntranetLinks(): Promise<IntranetLinks[]> {
		let booLoginOK: boolean = await this.checkLogin();
		if (booLoginOK) {
			const oHeaders = new Headers({ Authorization: 'Bearer ' + this.strToken });
			const oRequestOptions = new RequestOptions({ headers: oHeaders });
			var oResponseLink = await this.oHttp
				.post(
					this.strAppURL + 'common/api/Common/getIntranetLinks',
					{ ProgramName: 'TableBuilders' },
					oRequestOptions
				)
				.toPromise();
			return <IntranetLinks[]>oResponseLink.json();
		} else {
			return;
		}
	}

	async reverifyAuthyCode(AuthyCode: string): Promise<boolean> {
		let booLoginOK: boolean = await this.checkLogin();
		if (booLoginOK) {
			const oHeaders = new Headers({ Authorization: 'Bearer ' + this.strToken });
			const oRequestOptions = new RequestOptions({ headers: oHeaders });
			var oResponseLink = await this.oHttp
				.post(this.strAppURL + 'common/api/common/ReverityAuthyCode', { AuthyCode: AuthyCode }, oRequestOptions)
				.toPromise();
			let strReply: string = <string>oResponseLink.json();
			if (strReply == 'ok') {
				return true;
			} else {
				return false;
			}
		} else {
			return;
		}
	}

	/**
  Logs the user in. Returns true if successful.  If successful it also saves
  data about the user for the User object.
  */
	async login(Username: string, Password: string, AuthyToken: string, Phone: string): Promise<string> {
		try {
			const oResponseURL = await this.oHttp
				.get('https://www.adeq.state.ar.us/webapis/common/api/login/GetAppURL')
				.toPromise();
			this.strAppURL = oResponseURL.json();
			const oTokenResponse = await this.oHttp
				.post(this.strAppURL + 'common/api/login/Authenticate', {
					Username: Username,
					Password: Password,
					AuthyToken: AuthyToken,
					Phone: Phone,
					Email: AuthyToken
				})
				.toPromise();
			let strToken = oTokenResponse.json();

			await this.oStorage.set('username', Username);
			if (strToken == 'need phone') {
				strToken = '';
				return 'need phone';
			} else if (strToken == 'need authy') {
				strToken = '';
				return 'need authy';
			}
			this.strToken = strToken;
			await this.oStorage.set('id_token', strToken);
			await this.oStorage.set('phone', Phone);
			await this.oStorage.set('appurl', this.strAppURL);
			await this.oStorage.set('program-name', 'TableBuilders');

			await this.init();

			this.booShowLoginForm = false;
			this.oEvents.publish('Login:success');
			return 'ok';
		} catch (e) {
			console.log('Error:', e.Message);
			this.booShowLoginForm = true;
			return 'error';
		}
	}

	async getLastUsedUsername(): Promise<string> {
		return await this.oStorage.get('username');
	}

	/**
  Check to make sure that the user is logged in and the jwt token is not expired.
  If the user is not logged in then the login window is displayed.
  Returns true if logged in false if not (or error).
  */
	async checkLogin(): Promise<boolean> {
		try {
			if (!this.strAppURL) {
				await this.init();
			}
			const strIdToken = await this.oStorage.get('id_token');
			if (!strIdToken) {
				await this.oStorage.set('id_token', '');
				this.booShowLoginForm = true;
				return false;
			} else {
				this.strToken = strIdToken;
				this.strAppURL = await this.oStorage.get('appurl');
				this.booShowLoginForm = false;
				return true;
			}
		} catch (e) {
			console.log('Error:', e.Message);
			this.booShowLoginForm = true;
			return false;
		}
	}
}
