import { Component } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UserProvider } from '../../providers/user/user';
@Component({
	selector: 'adeq-login',
	templateUrl: 'login.html'
})
export class LoginComponent {
	credentialsForm: FormGroup;
	token: string;
	booShowSpinner: boolean = false;
	booShowPhone: boolean = false;
	booShowAuthy: boolean = false;

	constructor(
		private oToastController: ToastController,
		private oFormBuilder: FormBuilder,
		public oUserProvider: UserProvider
	) {
		this.credentialsForm = this.oFormBuilder.group({
			username: [''],
			password: [''],
			authyToken: [''],
			phone: ['']
		});
	}

	async ionViewDidLoad() {
		this.oUserProvider.checkLogin().then(() => {
			this.oUserProvider.getLastUsedUsername().then((returnedUsername) => {
				this.credentialsForm.controls.username.setValue(returnedUsername);
			});
		});
	}

	async login() {
		this.booShowSpinner = true;
		let strReturn = await this.oUserProvider.login(
			this.credentialsForm.controls.username.value,
			this.credentialsForm.controls.password.value,
			this.credentialsForm.controls.authyToken.value,
			this.credentialsForm.controls.phone.value
		);
		if (strReturn == 'ok') {
			this.booShowSpinner = false;
		} else if (strReturn == 'need phone') {
			this.booShowPhone = true;
			this.booShowSpinner = false;
			return;
		} else if (strReturn == 'need authy') {
			this.booShowPhone = false;
			this.booShowAuthy = true;
			this.booShowSpinner = false;
			return;
		} else {
			let toast = this.oToastController.create({
				message: 'Error. Try again.',
				duration: 2000,
				position: 'middle'
			});
			toast.present();
			this.booShowSpinner = false;
		}
	}
}
