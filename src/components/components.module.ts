import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { LoginComponent } from './login/login';
@NgModule({
	declarations: [ LoginComponent ],
	imports: [ IonicModule ],
	exports: [ LoginComponent ]
})
export class ComponentsModule {}
