import { Component, NgZone, ViewChild } from '@angular/core';
import { Platform, ToastController, NavController, NavParams, ModalController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import {
	TableBuilderReturnKind,
	APIError,
	APISuccess,
	TableRecord,
	TableRecords,
	FieldRecord,
	FieldRecords,
	AdminConfigRecord,
	AdminConfigRecords,
	TableBuilderProvider,
	DataTable,
	DataRow,
	IDValue,
	IDValues,
	FieldTypes,
	EditScreenMode,
	WebApiGetWhatNames,
	WebApiUpdateWhatNames,
	WebApiUpdateActionNames,
	LookupValues,
	ItemID,
	TableRightRecords,
	TableRightRecord
} from '../../providers/table-builder/table-builder';
import { AlertController, Content, IonicPage } from 'ionic-angular';
import { AppSettings } from '../../providers/app-settings/app-settings';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs';
import { CodemirrorService } from '@nomadreservations/ngx-codemirror';
import { LookupPage } from '../lookup/lookup';
import { SelectItem } from 'primeng/api';

@IonicPage({ name: 'page-edit', segment: 'view/:id', defaultHistory: ['page-list'] })
@Component({ selector: 'page-edit', templateUrl: 'edit.html' })
export class EditPage {
	public strWindowMode: 'Edit' | 'Add' | 'View';
	public oUserTablePermissions: IDValues;
	public booSystemAdminPermision: boolean = false;
	public booEditorRightForTable: boolean = false;
	public booDesignerRightForTable: boolean = false;
	public booViewerRightForTable: boolean = false;
	public booEditorRightForTableGroup: boolean = false;
	public booDesignerRightForTableGroup: boolean = false;
	public booViewerRightForTableGroup: boolean = false;
	public strDataMode: EditScreenMode;
	public oSelectItemViewerRightsButtons: SelectItem[];
	public oSelectItemDesignerRightsButtons: SelectItem[];
	public oSelectItemEditorRightsButtons: SelectItem[];
	public strSelectedViewerRightsButtons: string;
	public strSelectedDesignerRightsButtons: string;
	public strSelectedEditorRightsButtons: string;
	public strWindowTitle: string = '';
	public strID: string = '';
	public strParentID: string = '';
	public booShowDeleteButton: boolean = false;
	public oDataRowRecord: DataRow = <DataRow>{};
	public oDataRowRecordOriginal: DataRow;
	public oTableRights: TableRightRecord[] = new Array();
	public strTableViewersUsernames: string = '';
	public strTableViewersFullnames: string = '';
	public strTableEditorsUsernames: string = '';
	public strTableEditorsFullnames: string = '';
	public strTableDesignersUsernames: string = '';
	public strTableDesignersFullnames: string = '';
	public oFieldRecords: FieldRecords;
	public oFieldRecord: FieldRecord;
	public oFieldOriginal: FieldRecord;
	public oConfigField: AdminConfigRecord;
	public oConfigFieldOriginal: AdminConfigRecord;
	public oTableRecord: TableRecord;
	public oTableRecordOriginal: TableRecord;
	public booLargeScreen: boolean = false;
	public booShowForm: boolean = false;
	public arrControlWarnings: { [id: string]: string } = <{ [id: string]: string }>{};
	public oFieldTypes: FieldTypes;
	public oFieldUniquenessTypes: FieldTypes;
	public oFieldRequiredTypes: FieldTypes;
	booShowSpinner: boolean = false;
	booIsApp: boolean = false;
	booSkipAskSave: boolean = false;
	browserOffline: Subscription;
	browserOnline: Subscription;
	appIsOnline: boolean = true;

	oSelectedLookupValuesCodeGroup: string[] = new Array();
	oSelectedLookupValuesLabelGroup: string[] = new Array();
	oOriginalSelectedLookupValuesCodeGroup: string[] = new Array();
	oOriginalSelectedLookupValuesLabelGroup: string[] = new Array();

	@ViewChild(Content) oIonicContent: Content;
	constructor(
		public oNavController: NavController,
		public oNavParams: NavParams,
		private oToastController: ToastController,
		public oPlatform: Platform,
		private oNgZone: NgZone,
		public oAlertController: AlertController,
		private oUserProvider: UserProvider,
		private oAppSettings: AppSettings,
		private oTableBuilderProvider: TableBuilderProvider,
		private _codeMirror: CodemirrorService,
		public oModalController: ModalController
	) {
		if (oNavParams.get('id')) {
			this.strID = oNavParams.get('id');
		} else {
			this.strID = '';
		}
		if (oNavParams.get('parentid')) {
			this.strParentID = oNavParams.get('parentid');
		} else {
			this.strParentID = '';
		}
		if (oNavParams.get('mode')) {
			this.strDataMode = oNavParams.get('mode');
		}
		this.oPlatform.ready().then((readySource) => {
			this.oNgZone.run(() => {
				if (this.oPlatform.width() > 480) this.booLargeScreen = true;
				if (this.oPlatform.is('core') || this.oPlatform.is('mobileweb')) {
					// we are running in a browser or mobile web browser
					this.booIsApp = false;
				} else {
					this.booIsApp = true;
				}
			});
		});

		this.browserOffline = Observable.fromEvent(window, 'offline').subscribe(() => {
			this.appIsOnline = false;
		});

		this.browserOnline = Observable.fromEvent(window, 'online').subscribe(() => {
			this.appIsOnline = true;
		});
		if (this.strDataMode == 'Data') {
			this.oDataRowRecord.Values = new Array();
		}
		this._codeMirror.instance$.subscribe((editor) => {
			console.log(editor.state);
		});

		this.oSelectItemViewerRightsButtons = [
			{ label: 'User List', value: 'User List' },
			{ label: 'Only Me', value: 'Only Me' },
			{ label: 'Everyone', value: 'Everyone' }
		];
		this.oSelectItemEditorRightsButtons = [
			{ label: 'User List', value: 'User List' },
			{ label: 'Only Me', value: 'Only Me' },
			{ label: 'Everyone', value: 'Everyone' }
		];

		this.oSelectItemDesignerRightsButtons = [
			{ label: 'User List', value: 'User List' },
			{ label: 'Only Me', value: 'Only Me' }
		];
		if (this.strDataMode == 'Table Group') {
			this.oSelectItemDesignerRightsButtons = [
				{ label: 'User List', value: 'User List' },
				{ label: 'Only Me', value: 'Only Me' },
				{ label: 'Everyone', value: 'Everyone' }
			];
		}
	}

	ionViewCanLeave() {
		if (this.booSkipAskSave) return true;
		let booAskSave: boolean = false;
		if (this.strDataMode == 'Table' || this.strDataMode == 'Table Group' || this.strDataMode == 'SQL') {
			if (this.strWindowMode == 'Add') {
				if (this.oTableRecord.Name) booAskSave = true;
				if (this.oTableRecord.Comment) booAskSave = true;
				if (this.oTableRecord.Trigger) booAskSave = true;
				if (this.oTableRecord.SQLDataView) booAskSave = true;
				if (this.oTableRecord.Pinned) booAskSave = true;
			} else if (this.strWindowMode == 'Edit') {
				if (!(!this.oTableRecordOriginal.Name && !this.oTableRecord.Name))
					if (this.oTableRecordOriginal.Name != this.oTableRecord.Name) booAskSave = true;
				if (!(!this.oTableRecordOriginal.Comment && !this.oTableRecord.Comment))
					if (this.oTableRecordOriginal.Comment != this.oTableRecord.Comment) booAskSave = true;
				if (!(!this.oTableRecordOriginal.Trigger && !this.oTableRecord.Trigger))
					if (this.oTableRecordOriginal.Trigger != this.oTableRecord.Trigger) booAskSave = true;
				if (!(!this.oTableRecordOriginal.SQLDataView && !this.oTableRecord.SQLDataView))
					if (this.oTableRecordOriginal.SQLDataView != this.oTableRecord.SQLDataView) booAskSave = true;
				if (!(!this.oTableRecordOriginal.Pinned && !this.oTableRecord.Pinned))
					if (this.oTableRecordOriginal.Pinned != this.oTableRecord.Pinned) booAskSave = true;
				if (!(!this.oTableRecordOriginal.PinnedYN && !this.oTableRecord.PinnedYN))
					if (this.oTableRecordOriginal.PinnedYN != this.oTableRecord.PinnedYN) booAskSave = true;
			} else if (this.strWindowMode == 'View') {
				return true;
			}
		}
		if (this.strDataMode == 'Field') {
			if (this.strWindowMode == 'Add') {
				if (this.oFieldRecord.Name) booAskSave = true;
				if (this.oFieldRecord.Type) booAskSave = true;
				if (this.oFieldRecord.Tooltip) booAskSave = true;
				if (this.oFieldRecord.UniquenessID) booAskSave = true;
			} else if (this.strWindowMode == 'Edit') {
				if (!(!this.oFieldOriginal.Name && !this.oFieldRecord.Name))
					if (this.oFieldOriginal.Name != this.oFieldRecord.Name) booAskSave = true;
				if (!(!this.oFieldOriginal.Type && !this.oFieldRecord.Type))
					if (this.oFieldOriginal.Type != this.oFieldRecord.Type) booAskSave = true;
				if (!(!this.oFieldOriginal.Tooltip && !this.oFieldRecord.Tooltip))
					if (this.oFieldOriginal.Tooltip != this.oFieldRecord.Tooltip) booAskSave = true;
				if (!(!this.oFieldOriginal.UniquenessID && !this.oFieldRecord.UniquenessID))
					if (this.oFieldOriginal.UniquenessID != this.oFieldRecord.UniquenessID) booAskSave = true;
			} else if (this.strWindowMode == 'View') {
				return true;
			}
		}
		if (this.strDataMode == 'Admin') {
			if (this.strWindowMode == 'Add') {
				if (this.oConfigField.ParentID) booAskSave = true;
				if (this.oConfigField.Type) booAskSave = true;
				if (this.oConfigField.Value) booAskSave = true;
				if (this.oConfigField.DataLinkID) booAskSave = true;
			} else if (this.strWindowMode == 'Edit') {
				if (!(!this.oConfigFieldOriginal.ParentID && !this.oConfigField.ParentID))
					if (this.oConfigFieldOriginal.ParentID != this.oConfigField.ParentID) booAskSave = true;
				if (!(!this.oConfigFieldOriginal.Type && !this.oConfigField.Type))
					if (this.oConfigFieldOriginal.Type != this.oConfigField.Type) booAskSave = true;
				if (!(!this.oConfigFieldOriginal.Value && !this.oConfigField.Value))
					if (this.oConfigFieldOriginal.Value != this.oConfigField.Value) booAskSave = true;
				if (!(!this.oConfigFieldOriginal.DataLinkID && !this.oConfigField.DataLinkID))
					if (this.oConfigFieldOriginal.DataLinkID != this.oConfigField.DataLinkID) booAskSave = true;
			} else if (this.strWindowMode == 'View') {
				return true;
			}
		} else if (this.strDataMode == 'Data') {
			if (this.strWindowMode == 'Add') {
				for (let oField of this.oFieldRecords.Values) {
					if (this.oDataRowRecord.Values[oField.Name]) booAskSave = true;
				}
			} else if (this.strWindowMode == 'Edit') {
				for (let oField of this.oFieldRecords.Values) {
					if (!(!this.oDataRowRecordOriginal.Values[oField.Name] && !this.oDataRowRecord.Values[oField.Name]))
						if (this.oDataRowRecordOriginal.Values[oField.Name] != this.oDataRowRecord.Values[oField.Name])
							booAskSave = true;
				}
			} else if (this.strWindowMode == 'View') {
				return true;
			}
		}

		if (booAskSave) {
			return new Promise((resolve, reject) => {
				const confirm = this.oAlertController.create({
					title: 'Save?',
					message: 'Save your changes?',
					buttons: [
						{
							text: 'Yes',
							handler: () => {
								this.addOrUpdateRecord();
								resolve();
							}
						},
						{
							text: 'No',
							handler: () => {
								resolve();
							}
						},
						{
							text: 'Cancel',
							handler: () => {
								reject();
							}
						}
					]
				});
				confirm.present();
			});
		} else return true;
	}
	async ionViewCanEnter() {
		let booLoginOK = await this.oUserProvider.checkLogin();
		if (!booLoginOK) return;
	}

	async ionViewDidEnter() {
		let booLoginOK = await this.oUserProvider.checkLogin();
		if (!booLoginOK) return;
		this.booShowSpinner = true;
		this.strWindowMode = 'View';
		if (this.strID) {
			this.strWindowMode = 'Edit';
			this.booShowDeleteButton = true;
			this.strWindowTitle = 'Edit';
			this.strWindowMode = 'Edit';
		} else {
			this.strWindowTitle = 'Add';
			this.strWindowMode = 'Add';
		}

		let strRightsID: string = '';
		let strGetWhatRights: WebApiGetWhatNames = 'user-table-rights';
		if (this.strDataMode == 'Table Group') {
			strRightsID = this.strID;
			strGetWhatRights = 'user-table-group-rights';
		} else if (this.strDataMode == 'Table') {
			strRightsID = this.strID;
			strGetWhatRights = 'user-table-rights';
		} else {
			strRightsID = this.strParentID;
			strGetWhatRights = 'user-table-rights';
		}

		let oAPIReturn:
			| TableRecords
			| TableRecord
			| FieldRecords
			| FieldRecord
			| FieldTypes
			| IDValue
			| IDValues
			| AdminConfigRecord
			| AdminConfigRecords
			| DataTable
			| DataRow
			| ItemID
			| LookupValues
			| TableRightRecords
			| TableRightRecord
			| APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
				strGetWhatRights,
				0,
				0,
				strRightsID,
				''
			);
		if (
			oAPIReturn.kind === TableBuilderReturnKind.ErrorKind &&
			oAPIReturn.WebAPIMessage == 'The source contains no DataRows.'
		) {
			this.oUserTablePermissions = null;
			this.booSystemAdminPermision = false;
			this.booEditorRightForTable = false;
			this.booDesignerRightForTable = false;
			this.booViewerRightForTable = false;
			this.booEditorRightForTableGroup = false;
			this.booDesignerRightForTableGroup = false;
			this.booViewerRightForTableGroup = false;
		} else if (oAPIReturn.kind === TableBuilderReturnKind.ErrorKind) {
			if (oAPIReturn.WebAPIMessage === undefined) {
				oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
			}
			let toast = this.oToastController.create({
				message: oAPIReturn.WebAPIMessage,
				duration: 2000,
				position: 'middle'
			});
			toast.present();
			this.oNavController.pop();
			this.booShowSpinner = false;
			return;
		} else if (oAPIReturn.kind === TableBuilderReturnKind.IDValuesKind) {
			this.oUserTablePermissions = oAPIReturn;
			this.booSystemAdminPermision = false;
			this.booEditorRightForTable = false;
			this.booDesignerRightForTable = false;
			this.booViewerRightForTable = false;
			this.booEditorRightForTableGroup = false;
			this.booDesignerRightForTableGroup = false;
			this.booViewerRightForTableGroup = false;
			for (let oPermission of this.oUserTablePermissions.Values) {
				if (oPermission.Value == 'system-admin') {
					this.booSystemAdminPermision = true;
				} else if (oPermission.Value == 'table-right-editor') {
					this.booEditorRightForTable = true;
				} else if (oPermission.Value == 'table-right-designer') {
					this.booDesignerRightForTable = true;
				} else if (oPermission.Value == 'table-right-viewer') {
					this.booViewerRightForTable = true;
				} else if (oPermission.Value == 'table-group-right-editor') {
					this.booEditorRightForTableGroup = true;
				} else if (oPermission.Value == 'table-group-right-designer') {
					this.booDesignerRightForTableGroup = true;
				} else if (oPermission.Value == 'table-group-right-viewer') {
					this.booViewerRightForTableGroup = true;
				}
			}
		}

		if (this.strWindowMode === 'Add') {
			if (this.strDataMode == 'Table' || this.strDataMode == 'Table Group' || this.strDataMode == 'SQL') {
				this.oTableRecord = <TableRecord>{};
				this.oTableRecord.ID = '{new}';
			} else if (this.strDataMode == 'Field') {
				this.oFieldRecord = <FieldRecord>{};
				this.oFieldRecord.ID = '{new}';
				this.oFieldRecord.Name = '';
				this.oFieldRecord.Tooltip = '';
				this.oFieldRecord.Type = '';
				this.oFieldRecord.TypeID = '';
				this.oFieldRecord.UniquenessID = '';
				this.oFieldRecord.RequiredTypeID = '';
				this.oFieldRecord.InsertHiddenYN = 'N';
				this.oFieldRecord.InsertHidden = false;
			} else if (this.strDataMode == 'Admin') {
				this.oConfigField = <AdminConfigRecord>{};
				this.oConfigField.ID = '{new}';
				this.oConfigField.ParentID = '';
				this.oConfigField.DataLinkID = '';
				this.oConfigField.Type = '';
				this.oConfigField.Value = '';
			} else if (this.strDataMode == 'Data') {
				this.oDataRowRecord = <DataRow>{};
				this.oDataRowRecord.Values = new Array();
			}
			this.strWindowTitle = 'Add';
			this.booShowDeleteButton = false;
		} else if (this.strWindowMode === 'Edit' || this.strWindowMode === 'View') {
			let strGetWhat: WebApiGetWhatNames;
			if (this.strDataMode == 'Table Group') {
				strGetWhat = 'table-group';
			} else if (this.strDataMode == 'Table') {
				strGetWhat = 'table-info';
			} else if (this.strDataMode == 'Field') {
				strGetWhat = 'field-info';
			} else if (this.strDataMode == 'Data') {
				strGetWhat = 'data-row';
			} else if (this.strDataMode == 'Admin') {
				strGetWhat = 'admin-config-record';
			} else if (this.strDataMode == 'SQL') {
				strGetWhat = 'data-view';
			}
			let oAPIReturn:
				| TableRecords
				| TableRecord
				| FieldRecords
				| FieldRecord
				| DataTable
				| DataRow
				| IDValue
				| IDValues
				| FieldTypes
				| AdminConfigRecord
				| AdminConfigRecords
				| LookupValues
				| ItemID
				| TableRightRecords
				| TableRightRecord
				| APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
					strGetWhat,
					0,
					0,
					this.strID,
					''
				);
			if (oAPIReturn.kind === TableBuilderReturnKind.ErrorKind) {
				if (oAPIReturn.WebAPIMessage === undefined) {
					oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
				}
				let toast = this.oToastController.create({
					message: oAPIReturn.WebAPIMessage,
					duration: 2000,
					position: 'middle'
				});
				toast.present();
				this.oNavController.pop();
				this.booShowSpinner = false;
				return;
			} else if (oAPIReturn.kind === TableBuilderReturnKind.TableRecordKind) {
				this.oTableRecord = oAPIReturn;
				this.oTableRecordOriginal = this.oTableRecord;
				if (oAPIReturn.PinnedYN == 'Y') {
					this.oTableRecord.Pinned = true;
				} else {
					this.oTableRecord.Pinned = false;
				}
			} else if (oAPIReturn.kind === TableBuilderReturnKind.FieldRecordKind) {
				this.oFieldRecord = oAPIReturn;
				this.oFieldOriginal = this.oFieldRecord;
			} else if (oAPIReturn.kind === TableBuilderReturnKind.AdminConfigRecordKind) {
				this.oConfigField = oAPIReturn;
				this.oConfigFieldOriginal = this.oConfigField;
			} else if (oAPIReturn.kind === TableBuilderReturnKind.DataRowKind) {
				this.oDataRowRecord = oAPIReturn;
				this.oDataRowRecordOriginal = this.oDataRowRecord;
			}
		}

		// if (this.strDataMode == 'Field') {
		// 	if (this.oFieldRecord.InsertHiddenYN == 'Y') {
		// 		this.oFieldRecord.InsertHidden = true;
		// 	} else {
		// 		this.oFieldRecord.InsertHidden = false;
		// 	}
		// 	let oAPIReturn:
		// 		| TableRecords
		// 		| TableRecord
		// 		| FieldRecords
		// 		| FieldRecord
		// 		| FieldTypes
		// 		| IDValue
		// 		| IDValues
		// 		| AdminConfigRecord
		// 		| AdminConfigRecords
		// 		| DataTable
		// 		| DataRow
		// 		| ItemID
		// 		| LookupValues
		// 		| TableRightRecords
		// 		| TableRightRecord
		// 		| APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
		// 			'field-types',
		// 			0,
		// 			0,
		// 			'',
		// 			''
		// 		);
		// 	if (oAPIReturn.kind === TableBuilderReturnKind.ErrorKind) {
		// 		if (oAPIReturn.WebAPIMessage === undefined) {
		// 			oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
		// 		}
		// 		let toast = this.oToastController.create({
		// 			message: oAPIReturn.WebAPIMessage,
		// 			duration: 2000,
		// 			position: 'middle'
		// 		});
		// 		toast.present();
		// 		this.oNavController.pop();
		// 		this.booShowSpinner = false;
		// 		return;
		// 	} else if (oAPIReturn.kind === TableBuilderReturnKind.FieldTypesKind) {
		// 		this.oFieldTypes = oAPIReturn;
		// 	}
		// }

		if (this.strDataMode == 'Field') {
			let oAPIReturn:
				| TableRecords
				| TableRecord
				| FieldRecords
				| FieldRecord
				| FieldTypes
				| IDValue
				| IDValues
				| AdminConfigRecord
				| AdminConfigRecords
				| DataTable
				| DataRow
				| LookupValues
				| ItemID
				| TableRightRecords
				| TableRightRecord
				| APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
					'field-uniqueness-types',
					0,
					0,
					'',
					''
				);
			if (oAPIReturn.kind === TableBuilderReturnKind.ErrorKind) {
				if (oAPIReturn.WebAPIMessage === undefined) {
					oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
				}
				let toast = this.oToastController.create({
					message: oAPIReturn.WebAPIMessage,
					duration: 2000,
					position: 'middle'
				});
				toast.present();
				this.oNavController.pop();
				this.booShowSpinner = false;
				return;
			} else if (oAPIReturn.kind === TableBuilderReturnKind.FieldTypesKind) {
				this.oFieldUniquenessTypes = oAPIReturn;
			}
		}

		if (this.strDataMode == 'Field') {
			let oAPIReturn:
				| TableRecords
				| TableRecord
				| FieldRecords
				| FieldRecord
				| FieldTypes
				| IDValue
				| IDValues
				| AdminConfigRecord
				| AdminConfigRecords
				| DataTable
				| DataRow
				| LookupValues
				| ItemID
				| TableRightRecords
				| TableRightRecord
				| APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
					'field-required-types',
					0,
					0,
					'',
					''
				);
			if (oAPIReturn.kind === TableBuilderReturnKind.ErrorKind) {
				if (oAPIReturn.WebAPIMessage === undefined) {
					oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
				}
				let toast = this.oToastController.create({
					message: oAPIReturn.WebAPIMessage,
					duration: 2000,
					position: 'middle'
				});
				toast.present();
				this.oNavController.pop();
				this.booShowSpinner = false;
				return;
			} else if (oAPIReturn.kind === TableBuilderReturnKind.FieldTypesKind) {
				this.oFieldRequiredTypes = oAPIReturn;
			}
		}

		if ((this.strDataMode == 'Table Group' || this.strDataMode == 'Table') && this.strWindowMode == 'Add') {
			this.booViewerRightForTable = true;
			this.booDesignerRightForTable = true;
			this.booEditorRightForTable = true;
			this.strSelectedDesignerRightsButtons = 'Only Me';
			this.strSelectedEditorRightsButtons = 'Only Me';
			this.strSelectedViewerRightsButtons = 'Everyone';
		} else if ((this.strDataMode == 'Table Group' || this.strDataMode == 'Table') && this.strWindowMode == 'Edit') {
			let strTableOrGroup: WebApiGetWhatNames = 'table-rights';
			if (this.strDataMode == 'Table Group') {
				strTableOrGroup = 'table-group-rights';
			}
			let oAPIReturn:
				| TableRecords
				| TableRecord
				| FieldRecords
				| FieldRecord
				| FieldTypes
				| IDValue
				| IDValues
				| AdminConfigRecord
				| AdminConfigRecords
				| DataTable
				| DataRow
				| LookupValues
				| ItemID
				| TableRightRecords
				| APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
					strTableOrGroup,
					0,
					0,
					this.strID,
					''
				);
			if (oAPIReturn.kind === TableBuilderReturnKind.ErrorKind) {
				if (oAPIReturn.WebAPIMessage === undefined) {
					oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
				}

				if (oAPIReturn.WebAPIMessage === 'The source contains no DataRows.') {
					this.booShowSpinner = false;
					return;
				}
				let toast = this.oToastController.create({
					message: oAPIReturn.WebAPIMessage,
					duration: 2000,
					position: 'middle'
				});
				toast.present();
				this.oNavController.pop();
				this.booShowSpinner = false;
				return;
			} else if (oAPIReturn.kind === TableBuilderReturnKind.TableRightRecordsKind) {
				this.oTableRights = oAPIReturn.TableRightRecs;
				this.booViewerRightForTable = false;
				this.booDesignerRightForTable = false;
				this.booEditorRightForTable = false;
				this.strSelectedDesignerRightsButtons = 'User List';
				this.strSelectedEditorRightsButtons = 'User List';
				this.strSelectedViewerRightsButtons = 'User List';
				for (let oRight of this.oTableRights) {
					if (oRight.Right == 'table-group-right-viewer' || oRight.Right == 'table-right-viewer') {
						if (!this.strTableViewersUsernames) {
							this.strTableViewersUsernames += oRight.Username;
							this.strTableViewersFullnames += oRight.UserFullname;
						} else {
							this.strTableViewersUsernames += ', ' + oRight.Username;
							this.strTableViewersFullnames += ', ' + oRight.UserFullname;
						}
						if (
							oRight.Username.trim().toLowerCase() ==
							this.oUserProvider.oUserInfo.UserName.trim().toLowerCase() ||
							oRight.Username.trim().toLowerCase() == 'everyone'
						) {
							this.booViewerRightForTable = true;
						}
					} else if (oRight.Right == 'table-group-right-editor' || oRight.Right == 'table-right-editor') {
						if (!this.strTableEditorsUsernames) {
							this.strTableEditorsUsernames += oRight.Username;
							this.strTableEditorsFullnames += oRight.UserFullname;
						} else {
							this.strTableEditorsUsernames += ', ' + oRight.Username;
							this.strTableEditorsFullnames += ', ' + oRight.UserFullname;
						}
						if (
							oRight.Username.trim().toLowerCase() ==
							this.oUserProvider.oUserInfo.UserName.trim().toLowerCase() ||
							oRight.Username.trim().toLowerCase() == 'everyone'
						) {
							this.booEditorRightForTable = true;
						}
					} else if (oRight.Right == 'table-group-right-designer' || oRight.Right == 'table-right-designer') {
						if (!this.strTableDesignersUsernames) {
							this.strTableDesignersUsernames += oRight.Username;
							this.strTableDesignersFullnames += oRight.UserFullname;
						} else {
							this.strTableDesignersUsernames += ', ' + oRight.Username;
							this.strTableDesignersFullnames += ', ' + oRight.UserFullname;
						}
						if (
							oRight.Username.trim().toLowerCase() ==
							this.oUserProvider.oUserInfo.UserName.trim().toLowerCase()
						) {
							this.booDesignerRightForTable = true;
						}
					}
				}
				if (
					this.strTableDesignersUsernames.trim().toLowerCase() ==
					this.oUserProvider.oUserInfo.UserName.toLowerCase()
				) {
					this.strSelectedDesignerRightsButtons = 'Only Me';
					this.strTableDesignersFullnames = this.oUserProvider.oUserInfo.FullName;
				} else if (this.strTableDesignersUsernames.trim().toLowerCase() == 'everyone') {
					this.strSelectedDesignerRightsButtons = 'Everyone';
					this.strTableDesignersUsernames = this.oUserProvider.oUserInfo.UserName;
					this.strTableDesignersFullnames = this.oUserProvider.oUserInfo.FullName;
				}
				if (
					this.strTableEditorsUsernames.trim().toLowerCase() ==
					this.oUserProvider.oUserInfo.UserName.toLowerCase()
				) {
					this.strSelectedEditorRightsButtons = 'Only Me';
					this.strTableEditorsFullnames = this.oUserProvider.oUserInfo.FullName;
				} else if (this.strTableEditorsUsernames.trim().toLowerCase() == 'everyone') {
					this.strSelectedEditorRightsButtons = 'Everyone';
					this.strTableEditorsUsernames = this.oUserProvider.oUserInfo.UserName;
					this.strTableEditorsFullnames = this.oUserProvider.oUserInfo.FullName;
				}
				if (
					this.strTableViewersUsernames.trim().toLowerCase() ==
					this.oUserProvider.oUserInfo.UserName.toLowerCase()
				) {
					this.strSelectedViewerRightsButtons = 'Only Me';
					this.strTableViewersFullnames = this.oUserProvider.oUserInfo.FullName;
				} else if (this.strTableViewersUsernames.trim().toLowerCase() == 'everyone') {
					this.strSelectedViewerRightsButtons = 'Everyone';
					this.strTableViewersUsernames = this.oUserProvider.oUserInfo.UserName;
					this.strTableViewersFullnames = this.oUserProvider.oUserInfo.FullName;
				}
			}
		}

		if (this.strDataMode == 'Data') {
			let oAPIReturn:
				| TableRecords
				| TableRecord
				| FieldRecords
				| FieldRecord
				| IDValue
				| IDValues
				| AdminConfigRecord
				| AdminConfigRecords
				| FieldTypes
				| DataTable
				| DataRow
				| LookupValues
				| ItemID
				| TableRightRecords
				| TableRightRecord
				| APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
					'table-field-info',
					0,
					0,
					this.strParentID,
					''
				);
			if (oAPIReturn.kind === TableBuilderReturnKind.ErrorKind) {
				if (oAPIReturn.WebAPIMessage === undefined) {
					oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
				}
				let toast = this.oToastController.create({
					message: oAPIReturn.WebAPIMessage,
					duration: 2000,
					position: 'middle'
				});
				toast.present();
				this.oNavController.pop();
				this.booShowSpinner = false;
				return;
			} else if (oAPIReturn.kind === TableBuilderReturnKind.FieldRecordsKind) {
				this.oFieldRecords = oAPIReturn;
				if (this.strWindowMode == 'Add') {
					for (let oField of this.oFieldRecords.Values) {
						this.oDataRowRecord.Values[oField.Name] = '';
					}
				}
				let strCode: string;
				let strLabel: string;
				let strCombined: string;
				for (let oField of this.oFieldRecords.Values) {
					strCode = '';
					strLabel = '';
					strCombined = '';
					if (oField.SQLCodeLookupID) {
						strCombined = <string>this.oDataRowRecord.Values[oField.Name];
						if (strCombined) {
							if (strCombined.indexOf('[[{{value-lookup-divider}}]]') > -1) {
								strCode = strCombined.split('[[{{value-lookup-divider}}]]')[0].trim();
								strLabel = strCombined.split('[[{{value-lookup-divider}}]]')[1].trim();
								if (strLabel && strCode) {
									this.oSelectedLookupValuesLabelGroup[oField.Name] = strLabel;
									this.oSelectedLookupValuesCodeGroup[oField.Name] = strCode;
									this.oOriginalSelectedLookupValuesLabelGroup[oField.Name] = strLabel;
									this.oOriginalSelectedLookupValuesCodeGroup[oField.Name] = strCode;
								}
							}
						}
					} else if (oField.ChoiceList && oField.CodeMultiSelection == 'Y') {
						if (this.oDataRowRecord.Values[oField.Name]) {
							this.oDataRowRecord.Values[oField.Name + '_choice_array'] = this.oDataRowRecord.Values[
								oField.Name
							].split(', ');
						} else {
							this.oDataRowRecord.Values[oField.Name + '_choice_array'] = new Array();
						}
					} else if (
						oField.TypeSystemName == 'date' ||
						oField.TypeSystemName == 'date-and-time' ||
						oField.TypeSystemName == 'date-and-time-future-only' ||
						oField.TypeSystemName == 'date-and-time-past-only' ||
						oField.TypeSystemName == 'date-future-only' ||
						oField.TypeSystemName == 'date-past-only'
					) {
						if (this.oDataRowRecord.Values[oField.Name]) {
							this.oDataRowRecord.Values[oField.Name] = new Date(this.oDataRowRecord.Values[oField.Name]);
						} else {
							this.oDataRowRecord.Values[oField.Name] = '';
						}
					}
				}
			}
		}

		if (this.strDataMode == 'Table') {
			if (!this.booDesignerRightForTable) {
				this.booShowDeleteButton = false;
				this.strWindowTitle = 'View';
				this.strWindowMode = 'View';
			}
		}

		this.booShowForm = true;
		this.booShowSpinner = false;
	}

	async deleteRecord() {
		let confirm = this.oAlertController.create({
			title: 'Delete Confirmation',
			message: 'Are you sure that you want to delete this record?',
			buttons: [
				{
					text: 'Yes',
					handler: () => {
						this.deleteRecordConfirmed();
					}
				},
				{
					text: 'No'
				}
			]
		});
		confirm.present();
	}

	async deleteRecordConfirmed() {
		let strDeleteWhat: WebApiUpdateWhatNames;
		let strID: string = '';
		if (this.strDataMode == 'Table Group') {
			strDeleteWhat = 'table-group';
			strID = this.oTableRecord.ID;
		} else if (this.strDataMode == 'Table') {
			strDeleteWhat = 'table';
			strID = this.oTableRecord.ID;
		} else if (this.strDataMode == 'Field') {
			strDeleteWhat = 'field';
			strID = this.oFieldRecord.ID;
		} else if (this.strDataMode == 'Admin') {
			strDeleteWhat = 'admin';
			strID = this.oConfigField.ID;
		} else if (this.strDataMode == 'Data') {
			strDeleteWhat = 'data';
			strID = this.strID;
		}
		const oAPIReturn:
			| APISuccess
			| APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.updateData(
				strDeleteWhat,
				'delete',
				strID,
				'',
				this.oUserProvider.oUserInfo.UserName,
				this.oUserProvider.oUserInfo.EmployeeID
			);

		if (oAPIReturn.kind === TableBuilderReturnKind.SuccessKind) {
			this.oNavController.pop();
			return;
		} else if (oAPIReturn.WebAPIMessageType === 'NotLoggedIn') {
			return;
		} else if (oAPIReturn.WebAPIMessageType === 'GetAuthyCode') {
			let alert = this.oAlertController.create({
				title: this.oAppSettings.AuthyOneTouchStatement,
				buttons: [
					{
						text: 'OK',
						handler: () => {
							this.deleteRecordConfirmed();
							return;
						}
					}
				]
			});
			alert.present();
		} else {
			if (oAPIReturn.WebAPIMessage === undefined) {
				oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
			}
			let toast = this.oToastController.create({
				message: oAPIReturn.WebAPIMessage,
				duration: 2000,
				position: 'middle'
			});
			toast.present();
		}
	}

	async addOrUpdateRecord() {
		if (!this.strID) {
			// Add
			await this.addRecord();
		} else {
			// Update
			await this.updateRecord();
		}
	}

	async addRecord() {
		if (!this.inputIsValid()) {
			let toast = this.oToastController.create({
				message: 'Unable to save data until errors are fixed.',
				duration: 2000,
				position: 'middle'
			});
			toast.present();
			return;
		}
		let strAction: WebApiUpdateActionNames = 'insert';
		let strAddWhat: WebApiUpdateWhatNames;
		let strParentID: string = '';
		let strValue: string = '';

		if (this.strDataMode == 'Table Group' || this.strDataMode == 'Table') {
			strAddWhat = 'table-group';
			if (this.strDataMode == 'Table') {
				strAddWhat = 'table';
				strParentID = this.strParentID;
			}
			if (!this.oTableRecord.Comment) {
				this.oTableRecord.Comment = '';
			}
			if (!this.oTableRecord.Trigger) {
				this.oTableRecord.Trigger = '';
			}
			strValue += this.oTableRecord.Name.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			strValue += this.oTableRecord.Comment.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			strValue += this.oTableRecord.Trigger.trim().replace(/,/g, '[[{{comma}}]]') + ', ';

			if (this.strDataMode == 'Table Group') {
				if (this.oTableRecord.Pinned) {
					strValue += 'Y, ';
				} else {
					strValue += 'N, ';
				}
			}

			if (this.strSelectedEditorRightsButtons == 'Only Me') {
				strValue += 'e' + this.oUserProvider.oUserInfo.UserName + ', ';
			} else if (this.strSelectedEditorRightsButtons == 'Everyone') {
				strValue += 'eEveryone, ';
			} else if (this.strSelectedEditorRightsButtons == 'User List') {
				if (this.strTableEditorsUsernames) {
					for (let strRight of this.strTableEditorsUsernames.split(', ')) {
						strValue += 'e' + strRight.trim() + ', ';
					}
				}
			}

			if (this.strSelectedViewerRightsButtons == 'Only Me') {
				strValue += 'v' + this.oUserProvider.oUserInfo.UserName + ', ';
			} else if (this.strSelectedViewerRightsButtons == 'Everyone') {
				strValue += 'vEveryone, ';
			} else if (this.strSelectedViewerRightsButtons == 'User List') {
				if (this.strTableViewersUsernames) {
					for (let strRight of this.strTableViewersUsernames.split(', ')) {
						strValue += 'v' + strRight.trim() + ', ';
					}
				}
			}

			if (this.strSelectedDesignerRightsButtons == 'Only Me') {
				strValue += 'd' + this.oUserProvider.oUserInfo.UserName + ', ';
			} else if (this.strSelectedDesignerRightsButtons == 'Everyone') {
				strValue += 'dEveryone, ';
			} else if (this.strSelectedDesignerRightsButtons == 'User List') {
				if (this.strTableDesignersUsernames) {
					for (let strRight of this.strTableDesignersUsernames.split(', ')) {
						strValue += 'd' + strRight.trim() + ', ';
					}
				}
			}
		} else if (this.strDataMode == 'SQL') {
			strAddWhat = 'data-view';
			strParentID = this.strParentID;
			if (!this.oTableRecord.Comment) {
				this.oTableRecord.Comment = '';
			}
			if (!this.oTableRecord.SQLDataView) {
				this.oTableRecord.SQLDataView = '';
			}
			strValue =
				this.oTableRecord.Name.trim().replace(/,/g, '[[{{comma}}]]') +
				', ' +
				this.oTableRecord.Comment.trim().replace(/,/g, '[[{{comma}}]]') +
				', ' +
				this.oTableRecord.SQLDataView.trim().replace(/,/g, '[[{{comma}}]]');
		} else if (this.strDataMode == 'Field') {
			strAddWhat = 'field';
			strParentID = this.strParentID;
			strValue += strParentID.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			strValue += this.oFieldRecord.TypeID.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			strValue += this.oFieldRecord.TypeSystemName.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			if (this.oFieldRecord.TypeSystemName == 'choice') {
				strValue += this.oFieldRecord.ChoiceList.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			}
			if (this.oFieldRecord.CodeMultiSelection) {
				strValue += '__[[{{multi-code-selection}}]]__, ';
			}
			strValue += this.oFieldRecord.Name.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			strValue += this.oFieldRecord.Tooltip.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			strValue += this.oFieldRecord.UniquenessID.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			strValue += this.oFieldRecord.RequiredTypeID.trim().replace(/,/g, '[[{{comma}}]]');
			if (this.oFieldRecord.InsertHidden) {
				strValue += ', Y';
			} else {
				strValue += ', N';
			}
		} else if (this.strDataMode == 'Admin') {
			strAddWhat = 'admin';
			strValue =
				this.oConfigField.ParentID.trim().replace(/,/g, '[[{{comma}}]]') +
				', ' +
				this.oConfigField.Type.trim().replace(/,/g, '[[{{comma}}]]') +
				', ' +
				this.oConfigField.Value.trim().replace(/,/g, '[[{{comma}}]]') +
				', ' +
				this.oConfigField.DataLinkID.trim().replace(/,/g, '[[{{comma}}]]');
		} else if (this.strDataMode == 'Data') {
			let strSelectedCode: string = '';
			let strSelectedLabel: string = '';
			strAddWhat = 'data';
			strParentID = this.strParentID;

			for (let oField of this.oFieldRecords.Values) {
				if (oField.TypeSystemName != 'section-title') {
					if (oField.SQLCodeLookupID) {
						if (this.oSelectedLookupValuesCodeGroup[oField.Name]) {
							strSelectedCode = this.oSelectedLookupValuesCodeGroup[oField.Name];
						} else {
							strSelectedCode = '';
						}
						if (this.oSelectedLookupValuesLabelGroup[oField.Name]) {
							strSelectedLabel = this.oSelectedLookupValuesLabelGroup[oField.Name];
						} else {
							strSelectedLabel = '';
						}
						strValue = strValue.concat(
							this.zeroPadStringStart(oField.ID, 8) +
							strSelectedCode.trim().replace(/,/g, '[[{{comma}}]]') +
							', '
						);
						strValue = strValue.concat(
							this.zeroPadStringStart(oField.ID, 8) +
							'[[{{system-sql-lookup}}]]' +
							strSelectedLabel.trim().replace(/,/g, '[[{{comma}}]]') +
							', '
						);
					} else {
						if (!this.oDataRowRecord.Values[oField.Name]) {
							strValue = strValue.concat(this.zeroPadStringStart(oField.ID, 8) + ', ');
						} else if (this.oDataRowRecord.Values[oField.Name] instanceof Date) {
							strValue = strValue.concat(
								this.zeroPadStringStart(oField.ID, 8) +
								this.oDataRowRecord.Values[oField.Name]
									.toLocaleString()
									.trim()
									.replace(', 12:00:00 AM', '')
									.replace(/,/g, '[[{{comma}}]]') +
								', '
							);
						} else {
							strValue = strValue.concat(
								this.zeroPadStringStart(oField.ID, 8) +
								this.oDataRowRecord.Values[oField.Name].trim().replace(/,/g, '[[{{comma}}]]') +
								', '
							);
						}
					}
				}
			}
		}

		const oAPIReturn:
			| APISuccess
			| APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.updateData(
				strAddWhat,
				strAction,
				strParentID,
				strValue,
				this.oUserProvider.oUserInfo.UserName,
				this.oUserProvider.oUserInfo.EmployeeID
			);
		if (oAPIReturn.kind === TableBuilderReturnKind.SuccessKind) {
			this.oNavController.pop();
			this.booSkipAskSave = true;
			return;
		} else if (oAPIReturn.WebAPIMessageType === 'NotLoggedIn') {
			return;
		} else if (oAPIReturn.WebAPIMessageType === 'GetAuthyCode') {
			let alert = this.oAlertController.create({
				title: this.oAppSettings.AuthyOneTouchStatement,
				buttons: [
					{
						text: 'OK',
						handler: () => {
							this.addRecord();
							return;
						}
					}
				]
			});
			alert.present();
		} else {
			if (oAPIReturn.WebAPIMessage === undefined) {
				oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
			}
			let toast = this.oToastController.create({
				message: oAPIReturn.WebAPIMessage,
				duration: 2000,
				position: 'middle'
			});
			toast.present();
		}
	}

	zeroPadStringStart(StringNumber: string, StringLength: number): string {
		let strPaddedString: string = StringNumber;
		while (strPaddedString.length < StringLength) strPaddedString = '0' + strPaddedString;
		return strPaddedString;
	}

	inputIsValid(): boolean {
		for (let key in this.arrControlWarnings) {
			let value = this.arrControlWarnings[key];
			if (value.length > 0) return false;
		}
		return true;
	}

	async updateRecord() {
		if (!this.inputIsValid()) {
			let toast = this.oToastController.create({
				message: 'Unable to save data until errors are fixed.',
				duration: 2000,
				position: 'middle'
			});
			toast.present();
			return;
		}
		let strAction: WebApiUpdateActionNames = 'update';
		let strUpdateWhat: WebApiUpdateWhatNames;
		let strGetWhat: WebApiGetWhatNames;
		let oRecordCompare:
			| TableRecords
			| TableRecord
			| FieldRecords
			| FieldRecord
			| FieldTypes
			| IDValue
			| IDValues
			| DataTable
			| DataRow
			| AdminConfigRecord
			| AdminConfigRecords
			| LookupValues
			| ItemID
			| TableRightRecords
			| TableRightRecord
			| APIError;
		var booErrors: boolean = false;

		if (
			this.strDataMode == 'Table Group' ||
			this.strDataMode == 'Table' ||
			// this.strDataMode == 'Data' ||
			this.strDataMode == 'SQL'
		) {
			if (this.strDataMode == 'Table Group') {
				strUpdateWhat = 'table-group';
				strGetWhat = 'table-group';
			} else if (this.strDataMode == 'Table') {
				strUpdateWhat = 'table';
				strGetWhat = 'table-info';
				// } else if (this.strDataMode == 'Data') {
				// 	strUpdateWhat = 'data';
				// 	strGetWhat = 'data-row';
			} else if (this.strDataMode == 'SQL') {
				strUpdateWhat = 'data-view';
				strGetWhat = 'data-view';
			}

			oRecordCompare = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
				strGetWhat,
				0,
				0,
				this.strID,
				''
			);
			if (oRecordCompare.kind === TableBuilderReturnKind.ErrorKind) {
				if (oRecordCompare.WebAPIMessage === undefined) {
					oRecordCompare.WebAPIMessage = 'Error: No connection to server.';
				}
				let toast = this.oToastController.create({
					message: oRecordCompare.WebAPIMessage,
					duration: 2000,
					position: 'middle'
				});
				toast.present();
				return;
			}
			// if (this.strDataMode == 'Table' || this.strDataMode == 'Table Group')
			oRecordCompare = <TableRecord>oRecordCompare;
			if (oRecordCompare.PinnedYN == 'Y') {
				oRecordCompare.Pinned = true;
			} else {
				oRecordCompare.Pinned = false;
			}
			if (
				oRecordCompare.Name !== this.oTableRecordOriginal.Name &&
				this.oTableRecordOriginal.Name !== this.oTableRecord.Name
			) {
				this.arrControlWarnings['Name'] =
					'Name was also changed to ' + oRecordCompare.Name + ' by ' + this.oTableRecord.CreatedBy;
				'.';
				this.oTableRecordOriginal.Name = oRecordCompare.Name;
				booErrors = true;
			}
			if (
				oRecordCompare.Comment !== this.oTableRecordOriginal.Comment &&
				this.oTableRecordOriginal.Comment !== this.oTableRecord.Comment
			) {
				this.arrControlWarnings['Comment'] =
					'Comments was also changed to ' + oRecordCompare.Comment + ' by ' + this.oTableRecord.CreatedBy;
				'.';
				this.oTableRecordOriginal.Comment = oRecordCompare.Comment;
				booErrors = true;
			}
			if (
				oRecordCompare.Pinned !== this.oTableRecordOriginal.Pinned &&
				this.oTableRecordOriginal.Pinned !== this.oTableRecord.Pinned
			) {
				this.arrControlWarnings['Pinned'] =
					'Pinned was also changed to ' + oRecordCompare.Pinned + ' by ' + this.oTableRecord.CreatedBy;
				'.';
				this.oTableRecordOriginal.Pinned = oRecordCompare.Pinned;
				booErrors = true;
			}
			if (
				oRecordCompare.Trigger !== this.oTableRecordOriginal.Trigger &&
				this.oTableRecordOriginal.Trigger !== this.oTableRecord.Trigger
			) {
				this.arrControlWarnings['Trigger'] =
					'Trigger was also changed to ' + oRecordCompare.Trigger + ' by ' + this.oTableRecord.CreatedBy;
				'.';
				this.oTableRecordOriginal.Trigger = oRecordCompare.Trigger;
				booErrors = true;
			}
			if (
				oRecordCompare.SQLDataView !== this.oTableRecordOriginal.SQLDataView &&
				this.oTableRecordOriginal.SQLDataView !== this.oTableRecord.SQLDataView
			) {
				this.arrControlWarnings['SQLDataView'] =
					'Data View was also changed to ' +
					oRecordCompare.SQLDataView +
					' by ' +
					this.oTableRecord.CreatedBy;
				'.';
				this.oTableRecordOriginal.SQLDataView = oRecordCompare.SQLDataView;
				booErrors = true;
			}
		} else if (this.strDataMode == 'Field') {
			strUpdateWhat = 'field';
			strGetWhat = 'field-info';

			oRecordCompare = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
				strGetWhat,
				0,
				0,
				this.strID,
				''
			);
			if (oRecordCompare.kind === TableBuilderReturnKind.ErrorKind) {
				if (oRecordCompare.WebAPIMessage === undefined) {
					oRecordCompare.WebAPIMessage = 'Error: No connection to server.';
				}
				let toast = this.oToastController.create({
					message: oRecordCompare.WebAPIMessage,
					duration: 2000,
					position: 'middle'
				});
				toast.present();
				return;
			}

			oRecordCompare = <FieldRecord>oRecordCompare;
			if (
				oRecordCompare.Name !== this.oFieldOriginal.Name &&
				this.oFieldOriginal.Name !== this.oFieldRecord.Name
			) {
				this.arrControlWarnings['Name'] =
					'Name was also changed to ' + oRecordCompare.Name + ' by ' + this.oFieldRecord.CreatedBy;
				'.';
				this.oFieldOriginal.Name = oRecordCompare.Name;
				booErrors = true;
			}
			if (
				oRecordCompare.Type !== this.oFieldOriginal.Type &&
				this.oFieldOriginal.Type !== this.oFieldRecord.Type
			) {
				this.arrControlWarnings['Type'] =
					'Type was also changed to ' + oRecordCompare.Type + ' by ' + this.oFieldRecord.CreatedBy;
				'.';
				this.oFieldOriginal.Type = oRecordCompare.Type;
				booErrors = true;
			}
			if (
				oRecordCompare.Tooltip !== this.oFieldOriginal.Tooltip &&
				this.oFieldOriginal.Tooltip !== this.oFieldRecord.Tooltip
			) {
				this.arrControlWarnings['Tooltip'] =
					'Tooltip was also changed to ' + oRecordCompare.Tooltip + ' by ' + this.oFieldRecord.CreatedBy;
				'.';
				this.oFieldOriginal.Tooltip = oRecordCompare.Tooltip;
				booErrors = true;
			}
			if (
				oRecordCompare.UniquenessID !== this.oFieldOriginal.UniquenessID &&
				this.oFieldOriginal.UniquenessID !== this.oFieldRecord.UniquenessID
			) {
				this.arrControlWarnings['UniquenessID'] =
					'UniquenessID was also changed to ' +
					oRecordCompare.UniquenessID +
					' by ' +
					this.oFieldRecord.CreatedBy;
				'.';
				this.oFieldOriginal.UniquenessID = oRecordCompare.UniquenessID;
				booErrors = true;
			}
			if (
				oRecordCompare.RequiredTypeID !== this.oFieldOriginal.RequiredTypeID &&
				this.oFieldOriginal.RequiredTypeID !== this.oFieldRecord.RequiredTypeID
			) {
				this.arrControlWarnings['RequiredTypeID'] =
					'RequiredTypeID was also changed to ' +
					oRecordCompare.RequiredTypeID +
					' by ' +
					this.oFieldRecord.CreatedBy;
				'.';
				this.oFieldOriginal.RequiredTypeID = oRecordCompare.RequiredTypeID;
				booErrors = true;
			}
			if (
				oRecordCompare.InsertHidden !== this.oFieldOriginal.InsertHidden &&
				this.oFieldOriginal.InsertHidden !== this.oFieldRecord.InsertHidden
			) {
				this.arrControlWarnings['InsertHidden'] =
					'InsertHidden was also changed to ' +
					oRecordCompare.InsertHidden +
					' by ' +
					this.oFieldRecord.CreatedBy;
				'.';
				this.oFieldOriginal.InsertHidden = oRecordCompare.InsertHidden;
				booErrors = true;
			}
		} else if (this.strDataMode == 'Admin') {
			strUpdateWhat = 'admin';
			strGetWhat = 'admin-config-record';

			oRecordCompare = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
				strGetWhat,
				0,
				0,
				this.strID,
				''
			);
			if (oRecordCompare.kind === TableBuilderReturnKind.ErrorKind) {
				if (oRecordCompare.WebAPIMessage === undefined) {
					oRecordCompare.WebAPIMessage = 'Error: No connection to server.';
				}
				let toast = this.oToastController.create({
					message: oRecordCompare.WebAPIMessage,
					duration: 2000,
					position: 'middle'
				});
				toast.present();
				return;
			}

			oRecordCompare = <AdminConfigRecord>oRecordCompare;
			if (
				oRecordCompare.ParentID !== this.oConfigFieldOriginal.ParentID &&
				this.oConfigFieldOriginal.ParentID !== this.oConfigField.ParentID
			) {
				this.arrControlWarnings['ParentID'] =
					'Parent ID was also changed to ' + oRecordCompare.ParentID + ' by ' + this.oConfigField.CreatedBy;
				'.';
				this.oConfigFieldOriginal.ParentID = oRecordCompare.ParentID;
				booErrors = true;
			}
			if (
				oRecordCompare.Type !== this.oConfigFieldOriginal.Type &&
				this.oConfigFieldOriginal.Type !== this.oConfigField.Type
			) {
				this.arrControlWarnings['Type'] =
					'Type was also changed to ' + oRecordCompare.Type + ' by ' + this.oConfigField.CreatedBy;
				'.';
				this.oConfigFieldOriginal.Type = oRecordCompare.Type;
				booErrors = true;
			}
			if (
				oRecordCompare.Value !== this.oConfigFieldOriginal.Value &&
				this.oConfigFieldOriginal.Value !== this.oConfigField.Value
			) {
				this.arrControlWarnings['Value'] =
					'Value was also changed to ' + oRecordCompare.Value + ' by ' + this.oConfigField.CreatedBy;
				'.';
				this.oConfigFieldOriginal.Value = oRecordCompare.Value;
				booErrors = true;
			}
			if (
				oRecordCompare.DataLinkID !== this.oConfigFieldOriginal.DataLinkID &&
				this.oConfigFieldOriginal.DataLinkID !== this.oConfigField.DataLinkID
			) {
				this.arrControlWarnings['DataLinkID'] =
					'Data Link ID was also changed to ' +
					oRecordCompare.DataLinkID +
					' by ' +
					this.oConfigField.CreatedBy;
				'.';
				this.oConfigFieldOriginal.DataLinkID = oRecordCompare.DataLinkID;
				booErrors = true;
			}
		} else if (this.strDataMode == 'Data') {
			strUpdateWhat = 'data';
			strGetWhat = 'data-row';

			oRecordCompare = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
				strGetWhat,
				0,
				0,
				this.strID,
				''
			);
			if (oRecordCompare.kind === TableBuilderReturnKind.ErrorKind) {
				if (oRecordCompare.WebAPIMessage === undefined) {
					oRecordCompare.WebAPIMessage = 'Error: No connection to server.';
				}
				let toast = this.oToastController.create({
					message: oRecordCompare.WebAPIMessage,
					duration: 2000,
					position: 'middle'
				});
				toast.present();
				return;
			}

			oRecordCompare = <DataRow>oRecordCompare;
			let strCodeCompare: string = '';
			let strLabelCompare: string = '';
			let strCombined: string = '';
			let strOriginalCodes: string = '';
			for (let oField of this.oFieldRecords.Values) {
				if (oField.SQLCodeLookupID) {
					strCombined = oRecordCompare.Values[oField.Name];
					if (!strCombined) {
						strCombined = '';
						strCodeCompare = '';
						strLabelCompare = '';
					}

					strOriginalCodes = this.oOriginalSelectedLookupValuesCodeGroup[oField.Name];
					if (!strOriginalCodes) {
						strOriginalCodes = '';
					}
					if (strCombined.indexOf('[[{{value-lookup-divider}}]]') > -1) {
						strCodeCompare = strCombined.split('[[{{value-lookup-divider}}]]')[0].trim();
						strLabelCompare = strCombined.split('[[{{value-lookup-divider}}]]')[1].trim();
					}

					if (
						strCodeCompare !== strOriginalCodes &&
						strOriginalCodes !== this.oSelectedLookupValuesCodeGroup[oField.Name]
					) {
						this.arrControlWarnings[oField.Name] =
							oField.Name +
							' was also changed to ' +
							strLabelCompare +
							' by ' +
							oRecordCompare.Values[oField.Name].ModifiedBy;
						'.';
						this.oDataRowRecordOriginal[oField.Name] = oRecordCompare.Values[oField.Name];
						this.oOriginalSelectedLookupValuesCodeGroup[oField.Name] = this.oSelectedLookupValuesCodeGroup[
							oField.Name
						];
						booErrors = true;
					}
				} else {
					if (
						oRecordCompare.Values[oField.Name] !== this.oDataRowRecordOriginal[oField.Name] &&
						this.oDataRowRecordOriginal[oField.Name] !== this.oDataRowRecord[oField.Name]
					) {
						this.arrControlWarnings[oField.Name] =
							oField.Name +
							' was also changed to ' +
							oRecordCompare.Values[oField.Name] +
							' by ' +
							oRecordCompare.Values[oField.Name].ModifiedBy;
						'.';
						this.oDataRowRecordOriginal[oField.Name] = oRecordCompare.Values[oField.Name];
						booErrors = true;
					}
				}
			}
		}

		// If there were errors then do not update the database.
		if (booErrors) return;
		let strValue: string = '';

		if (this.strDataMode == 'Table' || this.strDataMode == 'Table Group') {
			strUpdateWhat = 'table-group';
			if (this.strDataMode == 'Table') {
				strUpdateWhat = 'table';
			}
			strValue += this.oTableRecord.Name.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			strValue += this.oTableRecord.Comment.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			if (this.oTableRecord.Pinned) {
				strValue += 'Y, ';
			} else {
				strValue += 'N, ';
			}

			if (this.strDataMode == 'Table') {
				strValue += this.oTableRecord.Trigger.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			}

			if (this.strSelectedEditorRightsButtons == 'Only Me') {
				strValue += 'e' + this.oUserProvider.oUserInfo.UserName + ', ';
			} else if (this.strSelectedEditorRightsButtons == 'Everyone') {
				strValue += 'eEveryone, ';
			} else if (this.strSelectedEditorRightsButtons == 'User List') {
				if (this.strTableEditorsUsernames) {
					for (let strRight of this.strTableEditorsUsernames.split(', ')) {
						strValue += 'e' + strRight.trim() + ', ';
					}
				}
			}

			if (this.strSelectedViewerRightsButtons == 'Only Me') {
				strValue += 'v' + this.oUserProvider.oUserInfo.UserName + ', ';
			} else if (this.strSelectedViewerRightsButtons == 'Everyone') {
				strValue += 'vEveryone, ';
			} else if (this.strSelectedViewerRightsButtons == 'User List') {
				if (this.strTableViewersUsernames) {
					for (let strRight of this.strTableViewersUsernames.split(', ')) {
						strValue += 'v' + strRight.trim() + ', ';
					}
				}
			}

			if (this.strSelectedDesignerRightsButtons == 'Only Me') {
				strValue += 'd' + this.oUserProvider.oUserInfo.UserName + ', ';
			} else if (this.strSelectedDesignerRightsButtons == 'Everyone') {
				strValue += 'dEveryone, ';
			} else if (this.strSelectedDesignerRightsButtons == 'User List') {
				if (this.strTableDesignersUsernames) {
					for (let strRight of this.strTableDesignersUsernames.split(', ')) {
						strValue += 'd' + strRight.trim() + ', ';
					}
				}
			}
		} else if (this.strDataMode == 'SQL') {
			strUpdateWhat = 'data-view';
			strValue =
				this.oTableRecord.Name.trim().replace(/,/g, '[[{{comma}}]]') +
				', ' +
				this.oTableRecord.Comment.trim().replace(/,/g, '[[{{comma}}]]') +
				', ' +
				this.oTableRecord.SQLDataView.trim().replace(/,/g, '[[{{comma}}]]');
		} else if (this.strDataMode == 'Field') {
			strUpdateWhat = 'field';
			strValue += this.strID.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			strValue += this.oFieldRecord.TypeID.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			strValue += this.oFieldRecord.TypeSystemName.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			if (this.oFieldRecord.TypeSystemName == 'choice') {
				strValue += this.oFieldRecord.ChoiceList.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			}
			if (this.oFieldRecord.CodeMultiSelection) {
				strValue += '__[[{{multi-code-selection}}]]__, ';
			}
			strValue += this.oFieldRecord.Name.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			strValue += this.oFieldRecord.Tooltip.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			strValue += this.oFieldRecord.UniquenessID.trim().replace(/,/g, '[[{{comma}}]]') + ', ';
			strValue += this.oFieldRecord.RequiredTypeID.trim().replace(/,/g, '[[{{comma}}]]');
			if (this.oFieldRecord.InsertHidden) {
				strValue += ', Y';
			} else {
				strValue += ', N';
			}
		} else if (this.strDataMode == 'Admin') {
			strUpdateWhat = 'admin';
			strValue =
				this.oConfigField.ParentID.trim().replace(/,/g, '[[{{comma}}]]') +
				', ' +
				this.oConfigField.Type.trim().replace(/,/g, '[[{{comma}}]]') +
				', ' +
				this.oConfigField.Value.trim().replace(/,/g, '[[{{comma}}]]') +
				', ' +
				this.oConfigField.DataLinkID.trim().replace(/,/g, '[[{{comma}}]]');
		} else if (this.strDataMode == 'Data') {
			strUpdateWhat = 'data';
			let strSelectedCode: string = '';
			let strSelectedLabel: string = '';

			for (let oField of this.oFieldRecords.Values) {
				if (oField.SQLCodeLookupID) {
					strSelectedCode = this.oSelectedLookupValuesCodeGroup[oField.Name];
					strSelectedLabel = this.oSelectedLookupValuesLabelGroup[oField.Name];
					if (!strSelectedCode) {
						strSelectedCode = '';
					}
					if (!strSelectedLabel) {
						strSelectedLabel = '';
					}
					strValue = strValue.concat(
						this.zeroPadStringStart(oField.ID, 8) +
						strSelectedCode.trim().replace(/,/g, '[[{{comma}}]]') +
						', '
					);
					strValue = strValue.concat(
						this.zeroPadStringStart(oField.ID, 8) +
						'[[{{system-sql-lookup}}]]' +
						strSelectedLabel.trim().replace(/,/g, '[[{{comma}}]]') +
						', '
					);
				} else {
					if (!this.oDataRowRecord.Values[oField.Name]) {
						strValue = strValue.concat(this.zeroPadStringStart(oField.ID, 8) + ', ');
					} else if (this.oDataRowRecord.Values[oField.Name] instanceof Date) {
						strValue = strValue.concat(
							this.zeroPadStringStart(oField.ID, 8) +
							this.oDataRowRecord.Values[oField.Name]
								.toLocaleString()
								.trim()
								.replace(', 12:00:00 AM', '')
								.replace(/,/g, '[[{{comma}}]]') +
							', '
						);
					} else {
						strValue = strValue.concat(
							this.zeroPadStringStart(oField.ID, 8) +
							this.oDataRowRecord.Values[oField.Name].trim().replace(/,/g, '[[{{comma}}]]') +
							', '
						);
					}
				}
			}
		}

		let oAPIReturn = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.updateData(
			strUpdateWhat,
			strAction,
			this.strID,
			strValue,
			this.oUserProvider.oUserInfo.UserName,
			this.oUserProvider.oUserInfo.EmployeeID
		);
		if (oAPIReturn.kind === TableBuilderReturnKind.SuccessKind) {
			this.oNavController.pop();
			this.booSkipAskSave = true;
			return;
		} else if (oAPIReturn.WebAPIMessageType === 'NotLoggedIn') {
			return;
		} else if (oAPIReturn.WebAPIMessageType === 'GetAuthyCode') {
			let alert = this.oAlertController.create({
				title: this.oAppSettings.AuthyOneTouchStatement,
				buttons: [
					{
						text: 'OK',
						handler: () => {
							this.updateRecord();
							return;
						}
					}
				]
			});
			await alert.present();
		} else {
			if (oAPIReturn.WebAPIMessage === undefined) {
				oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
			}
			let toast = this.oToastController.create({
				message: oAPIReturn.WebAPIMessage,
				duration: 2000,
				position: 'middle'
			});
			toast.present();
		}
	}

	checkRegExFailureForField(FieldName: string, Value: string, TypeRegex: string, FailMessage: string): boolean {
		if (!TypeRegex) return false;
		if (!FailMessage) return false;
		if (!Value) return false;
		if (!FieldName) return false;

		let regexp = new RegExp(TypeRegex);
		if (!regexp.test(Value)) {
			this.arrControlWarnings[FieldName] = FailMessage;
			return true;
		}

		this.arrControlWarnings[FieldName] = '';
		return false;
	}

	showAuditInfo() {
		if (this.strDataMode == 'Table' || this.strDataMode == 'Table Group') {
			const oAlert = this.oAlertController.create({
				title: 'Audit Information',
				message: `Created by:
				${this.oTableRecord.CreatedBy || ''} <br/>
				Created Date:
				${this.oTableRecord.CreatedDate || ''} <br/>
				Modified by:
				${this.oTableRecord.ModifiedBy || ''} <br/>
				Modified Date:
				${this.oTableRecord.ModifiedDate || ''} <br/>
				`,
				buttons: [{ text: 'OK' }]
			});
			oAlert.present();
		} else if (this.strDataMode == 'Field') {
			const oAlert = this.oAlertController.create({
				title: 'Audit Information',
				message: `Created by:
				${this.oFieldRecord.CreatedBy || ''} <br/>
				Created Date:
				${this.oFieldRecord.CreatedDate || ''} <br/>
				Modified by:
				${this.oFieldRecord.ModifiedBy || ''} <br/>
				Modified Date:
				${this.oFieldRecord.ModifiedDate || ''} <br/>
				`,
				buttons: [{ text: 'OK' }]
			});
			oAlert.present();
		} else if (this.strDataMode == 'Data') {
			const oAlert = this.oAlertController.create({
				title: 'Audit Information',
				message: `Created by:
			    ${this.oDataRowRecord.Values['CreatedBy'] || ''} <br/>
			    Created Date:
			    ${this.oDataRowRecord.Values['CreatedDate'] || ''} <br/>
				Modified by:
				${this.oDataRowRecord.Values['ModifiedBy'] || ''} <br/>
				Modified Date:
				${this.oDataRowRecord.Values['ModifiedDate'] || ''} <br/>
				`,
				buttons: [{ text: 'OK' }]
			});
			oAlert.present();
		} else if (this.strDataMode == 'Admin') {
			const oAlert = this.oAlertController.create({
				title: 'Audit Information',
				message: `Created by:
				${this.oConfigField.CreatedBy || ''} <br/>
				Created Date:
				${this.oConfigField.CreatedDate || ''} <br/>
				Modified by:
				${this.oConfigField.ModifiedBy || ''} <br/>
				Modified Date:
				${this.oConfigField.ModifiedDate || ''} <br/>
				`,
				buttons: [{ text: 'OK' }]
			});
			oAlert.present();
		}
	}

	// onFieldTypeSelected() {
	// 	for (let oType of this.oFieldTypes.Values) {
	// 		if (this.oFieldRecord.TypeID == oType.ID) {
	// 			this.oFieldRecord.TypeSystemName = oType.SystemName;
	// 			this.oFieldRecord.SQLCodeLookupID = oType.SQLLookupID;
	// 		}
	// 	}
	// }

	async selectFieldType() {
		let modal = await this.oModalController.create(LookupPage, {
			strCodeList: this.oFieldRecord.TypeID,
			strLabelList: this.oFieldRecord.Type,
			LookupID: '----SelectFieldTypes----',
			multiple: 'false'
		});
		modal.onDidDismiss((ReturnedCodes, ReturnedLabels) => {
			this.oFieldRecord.TypeID = ReturnedCodes;
			this.oFieldRecord.Type = ReturnedLabels;
		});
		await modal.present();
	}

	async selectTableViewers() {
		if (this.strTableViewersUsernames.trim() == 'Everyone') {
			this.strTableViewersUsernames = '';
			this.strTableViewersFullnames = '';
		} else if (this.strTableViewersUsernames.trim() == 'Only Me') {
			this.strTableViewersUsernames = this.oUserProvider.oUserInfo.UserName;
			this.strTableViewersFullnames = this.oUserProvider.oUserInfo.FullName;
		}
		let modal = await this.oModalController.create(LookupPage, {
			strCodeList: this.strTableViewersUsernames,
			strLabelList: this.strTableViewersFullnames,
			LookupID: '----SelectUserRights----',
			multiple: 'true'
		});
		modal.onDidDismiss((ReturnedCodes, ReturnedLabels) => {
			// if (ReturnedCodes && ReturnedLabels) {
			this.strTableViewersUsernames = ReturnedCodes;
			this.strTableViewersFullnames = ReturnedLabels;
			// }
		});
		await modal.present();
	}

	async selectTableEditors() {
		if (this.strTableEditorsUsernames.trim() == 'Everyone') {
			this.strTableEditorsUsernames = '';
			this.strTableEditorsFullnames = '';
		} else if (this.strTableEditorsUsernames.trim() == 'Only Me') {
			this.strTableEditorsUsernames = this.oUserProvider.oUserInfo.UserName;
			this.strTableEditorsFullnames = this.oUserProvider.oUserInfo.FullName;
		}
		let modal = await this.oModalController.create(LookupPage, {
			strCodeList: this.strTableEditorsUsernames,
			strLabelList: this.strTableEditorsFullnames,
			LookupID: '----SelectUserRights----',
			multiple: 'true'
		});
		modal.onDidDismiss((ReturnedCodes, ReturnedLabels) => {
			// if (ReturnedCodes && ReturnedLabels) {
			this.strTableEditorsUsernames = ReturnedCodes;
			this.strTableEditorsFullnames = ReturnedLabels;
			// }
		});
		await modal.present();
	}

	async selectTableDesigners() {
		if (this.strTableDesignersUsernames.trim() == 'Everyone') {
			this.strTableDesignersUsernames = '';
			this.strTableDesignersFullnames = '';
		} else if (this.strTableDesignersUsernames.trim() == 'Only Me') {
			this.strTableDesignersUsernames = this.oUserProvider.oUserInfo.UserName;
			this.strTableDesignersFullnames = this.oUserProvider.oUserInfo.FullName;
		}
		let modal = await this.oModalController.create(LookupPage, {
			strCodeList: this.strTableDesignersUsernames,
			strLabelList: this.strTableDesignersFullnames,
			LookupID: '----SelectUserRights----',
			multiple: 'true'
		});
		modal.onDidDismiss((ReturnedCodes, ReturnedLabels) => {
			// if (ReturnedCodes && ReturnedLabels) {
			this.strTableDesignersUsernames = ReturnedCodes;
			this.strTableDesignersFullnames = ReturnedLabels;
			// }
		});
		await modal.present();
	}

	async selectItemFromLookup(Field: FieldRecord) {
		let strMultiple: string = 'true';
		if (Field.CodeMultiSelection == 'Y') {
			strMultiple = 'true';
		} else {
			strMultiple = 'false';
		}
		let modal = await this.oModalController.create(LookupPage, {
			strCodeList: this.oSelectedLookupValuesCodeGroup[Field.Name],
			strLabelList: this.oSelectedLookupValuesLabelGroup[Field.Name],
			LookupID: Field.SQLCodeLookupID,
			multiple: strMultiple
		});
		modal.onDidDismiss((ReturnedCodes, ReturnedLabels) => {
			// if (ReturnedCodes && ReturnedLabels) {
			this.oSelectedLookupValuesCodeGroup[Field.Name] = ReturnedCodes;
			this.oSelectedLookupValuesLabelGroup[Field.Name] = ReturnedLabels;
			// }
		});
		await modal.present();
	}

	onChoiceSelected(Field: FieldRecord) {
		if (
			Field.ChoiceList &&
			this.oDataRowRecord.Values[Field.Name + '_choice_array'] &&
			this.oDataRowRecord.Values[Field.Name + '_choice_array'].length > 0
		) {
			this.oDataRowRecord.Values[Field.Name] = this.oDataRowRecord.Values[Field.Name + '_choice_array'].join(
				', '
			);
		} else {
			this.oDataRowRecord.Values[Field.Name] = '';
		}
	}
}
