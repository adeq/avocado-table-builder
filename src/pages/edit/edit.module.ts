import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditPage } from './edit';
import { QuillModule } from 'ngx-quill-wrapper';
import { ComponentsModule } from '../../components/components.module';
import { CodemirrorModule } from '@nomadreservations/ngx-codemirror';
import 'codemirror/mode/sql/sql';
import { InputTextModule, ButtonModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/calendar';
import { SelectButtonModule } from 'primeng/selectbutton';
import { AccordionModule } from 'primeng/accordion';
import { PanelModule } from 'primeng/panel';
import { ToggleButtonModule } from 'primeng/togglebutton';

@NgModule({
	declarations: [EditPage],
	imports: [
		IonicPageModule.forChild(EditPage),
		QuillModule,
		ComponentsModule,
		CodemirrorModule,
		InputTextModule,
		ButtonModule,
		CalendarModule,
		SelectButtonModule,
		AccordionModule,
		PanelModule,
		ToggleButtonModule
	]
})
export class EditPageModule { }
