import { Component, NgZone } from '@angular/core';
import { NavParams, ViewController, ToastController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import {
	LookupValues,
	LookupValue,
	TableBuilderReturnKind,
	WebApiGetWhatNames,
	TableRecords,
	TableRecord,
	FieldRecords,
	FieldRecord,
	FieldTypes,
	IDValue,
	IDValues,
	AdminConfigRecord,
	AdminConfigRecords,
	DataTable,
	DataRow,
	ItemID,
	APIError,
	TableBuilderProvider,
	TableRightRecord,
	TableRightRecords
} from '../../providers/table-builder/table-builder';
@Component({
	selector: 'page-lookup',
	templateUrl: 'lookup.html'
})
export class LookupPage {
	public arrOriginalCodeList: LookupValues;
	public arrDisplayCodeList: LookupValues;
	public booSelectMultiple: boolean = false;
	intCurrentPage: number = 0;
	intRecordsPerPage: number = 100;
	public strParamSLookupID: string = '';
	public strArrSelectionCodeList: string[] = new Array();
	public strArrSelectionLabelList: string[] = new Array();
	strCurrentSearchString: string = '';

	constructor(
		private oNavParams: NavParams,
		private oViewController: ViewController,
		private oNgZone: NgZone,
		public oTableBuilderProvider: TableBuilderProvider,
		public oToastController: ToastController
	) {
		this.oNgZone.run(() => {
			this.booSelectMultiple = this.oNavParams.get('multiple') == 'true';
			let strParamSelectionCodeList: string = this.oNavParams.get('strCodeList');
			let strParamSelectionLabelList: string = this.oNavParams.get('strLabelList');
			this.strParamSLookupID = this.oNavParams.get('LookupID');
			if (strParamSelectionCodeList) this.strArrSelectionCodeList = strParamSelectionCodeList.split(', ');
			if (strParamSelectionLabelList) this.strArrSelectionLabelList = strParamSelectionLabelList.split(', ');

			this.arrDisplayCodeList = <LookupValues>{ kind: TableBuilderReturnKind.LookupValuesKind, Values: [] };

			for (let key in this.strArrSelectionCodeList) {
				this.arrDisplayCodeList.Values.push({
					kind: TableBuilderReturnKind.LookupValueKind,
					Code: this.strArrSelectionCodeList[key],
					Label: this.strArrSelectionLabelList[key],
					Selected: true
				});
			}
		});
	}

	async ionViewDidEnter() {
		this.intCurrentPage = 0;
		this.arrOriginalCodeList = <LookupValues>{};
		// this.arrDisplayCodeList = <LookupValues>{};
		this.arrOriginalCodeList.Values = <LookupValue[]>new Array();
		// this.arrDisplayCodeList.Values = <LookupValue[]>new Array();
		await this.getCodeList(this.strParamSLookupID, '', this.intCurrentPage, this.intRecordsPerPage);
	}

	closePage() {
		if (this.booSelectMultiple) {
			let strArrCodes: string[] = new Array();
			let strArrLabels: string[] = new Array();
			for (let oEach of this.arrDisplayCodeList.Values) {
				if (oEach.Selected) {
					strArrCodes.push(oEach.Code);
					strArrLabels.push(oEach.Label);
				}
			}
			this.oViewController.dismiss(strArrCodes.join(', '), strArrLabels.join(', '));
		} else {
			this.oViewController.dismiss();
		}
	}

	singleItemSelected(index: number) {
		this.oViewController.dismiss(
			this.arrDisplayCodeList.Values[index].Code,
			this.arrDisplayCodeList.Values[index].Label
		);
	}

	async doInfinite(oInfiniteScroll) {
		this.intCurrentPage = this.intCurrentPage + 1;
		await this.getCodeList(
			this.strParamSLookupID,
			this.strCurrentSearchString,
			this.intCurrentPage,
			this.intRecordsPerPage
		);
		oInfiniteScroll.complete();
	}

	async getSearch(ev: any) {
		this.intCurrentPage = 0;
		this.strCurrentSearchString = ev.target.value;

		await this.getCodeList(
			this.strParamSLookupID,
			this.strCurrentSearchString,
			this.intCurrentPage,
			this.intRecordsPerPage
		);
	}

	async getCodeList(SQLLookupID: string, SearchString: string, CurrentPage: number, RecordsPerPage: number) {
		let strGetWhat: WebApiGetWhatNames = 'sql-code-lookup-list';
		strGetWhat = 'sql-code-lookup-list';
		let oAPIReturn:
			| TableRecords
			| TableRecord
			| FieldRecords
			| FieldRecord
			| FieldTypes
			| IDValue
			| IDValues
			| AdminConfigRecord
			| AdminConfigRecords
			| DataTable
			| DataRow
			| ItemID
			| APIError
			| TableRightRecord
			| TableRightRecords
			| LookupValues = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
			strGetWhat,
			CurrentPage,
			RecordsPerPage,
			SQLLookupID,
			SearchString
		);
		if (
			oAPIReturn.kind === TableBuilderReturnKind.ErrorKind &&
			oAPIReturn.WebAPIMessage === 'The source contains no DataRows.'
		) {
			if (this.intCurrentPage === 0) {
				this.arrOriginalCodeList = null;
			} else {
				return;
			}
		} else if (
			oAPIReturn.kind === TableBuilderReturnKind.ErrorKind &&
			oAPIReturn.WebAPIMessageType === 'NotLoggedIn'
		) {
			return;
		} else if (oAPIReturn.kind === TableBuilderReturnKind.ErrorKind) {
			if (oAPIReturn.WebAPIMessage === undefined) {
				oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
			}
			let toast = this.oToastController.create({
				message: oAPIReturn.WebAPIMessage,
				duration: 2000,
				position: 'middle'
			});
			toast.present();
			return;
		} else if (oAPIReturn.kind === TableBuilderReturnKind.LookupValuesKind) {
			if (!this.arrOriginalCodeList) this.arrOriginalCodeList = <LookupValues>{};
			let oNewLookupValues: LookupValues = <LookupValues>{};
			oNewLookupValues.Values = new Array();
			this.strArrSelectionCodeList = <string[]>new Array();
			this.strArrSelectionLabelList = <string[]>new Array();
			for (let oEach of this.arrDisplayCodeList.Values) {
				if (oEach.Selected) {
					oNewLookupValues.Values.push({
						Label: String(oEach.Label),
						Code: String(oEach.Code),
						Selected: oEach.Selected,
						kind: TableBuilderReturnKind.LookupValueKind
					});
					this.strArrSelectionCodeList.push(String(oEach.Code));
					this.strArrSelectionLabelList.push(String(oEach.Label));
				}
			}
			for (let oEach of oAPIReturn.Values) {
				if (this.strArrSelectionCodeList.indexOf(String(oEach.Code)) == -1) {
					oNewLookupValues.Values.push({
						Label: String(oEach.Label),
						Code: String(oEach.Code),
						Selected: false,
						kind: TableBuilderReturnKind.LookupValueKind
					});
				}
			}
			this.oNgZone.run(() => {
				this.arrDisplayCodeList = oNewLookupValues;
			});
		}
	}
}
