import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LookupPage } from './lookup';
import { ComponentsModule } from '../../components/components.module';
@NgModule({
	declarations: [ LookupPage ],
	imports: [ IonicPageModule.forChild(LookupPage), ComponentsModule ]
})
export class LookupPageModule {}
