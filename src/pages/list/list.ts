
import { Component, NgZone, ViewChild } from '@angular/core';
import { Events, IonicPage, Platform, NavController, AlertController, ToastController, Content, NavParams } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { EditPage } from '../edit/edit';
import { TableBuilderReturnKind, APIError, APISuccess, TableRecord, TableRecords, FieldRecord, FieldRecords, AdminConfigRecord, AdminConfigRecords, DataTable, DataRow, IDValue, IDValues, TableBuilderProvider, WebApiGetWhatNames, EditScreenMode, ListScreenMode, LookupValues, ItemID, FieldTypes, TableRightRecord, TableRightRecords, WorkflowStepRecord, WorkflowStepRecords } from '../../providers/table-builder/table-builder';
import { UserProvider } from '../../providers/user/user';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs';
import { AppSettings } from '../../providers/app-settings/app-settings';
import { DomSanitizer } from '@angular/platform-browser';

export interface IntranetLinks {
    title: string;
    URL: string;
}

@IonicPage({ name: 'page-list', segment: 'data/:url-action' })
@Component({ selector: 'page-list', templateUrl: 'list.html' })
export class ListPage {
    booSystemAdminPermision: boolean = false;
    booViewerRightForTable: boolean = false;
    booEditorRightForTable: boolean = false;
    booDesignerRightForTable: boolean = false;
    booViewerRightForTableGroup: boolean = false;
    booEditorRightForTableGroup: boolean = false;
    booDesignerRightForTableGroup: boolean = false;
    oUserTablePermissions: IDValues;
    strMode: ListScreenMode = 'Table Groups';
    booLargeScreen: boolean = false;
    intCurrentPage: number = 0;
    intRecordsPerPage: number = 100;
    strCurrentSearchString: string = '';
    oRecords: TableRecord[] = [];
    oFieldRecords: string[];
    oAdminConfigRecords: AdminConfigRecord[];
    oDataTable: DataTable;
    strSelectedTableGroupID: string = '';
    strSelectedTableGroup: string = '';
    strID: string = '';
    strSelectedTable: string = '';
    booShowSpinner: boolean = false;
    oIntranetLinks: IntranetLinks[];
    browserOffline: Subscription;
    browserOnline: Subscription;
    appIsOnline: boolean = true;
    strExecuteSQLID: string = '';
    oEmailTriggerTimes: IDValues;
    @ViewChild(Content) oIonicContent: Content;

    constructor(
        public oNavController: NavController,
        private oPlatform: Platform,
        private oToastController: ToastController,
        private oNgZone: NgZone,
        private oTableBuilderProvider: TableBuilderProvider,
        private oAlertController: AlertController,
        private oUserProvider: UserProvider,
        private oEvents: Events,
        private oAppSettings: AppSettings,
        public oNavParams: NavParams,
        public oDomSanitizer: DomSanitizer
    ) {
        this.oPlatform.ready().then((readySource) => {
            this.oNgZone.run(async () => {
                if (this.oPlatform.width() > 480) this.booLargeScreen = true;
            });
        });
        this.browserOffline = Observable.fromEvent(window, 'offline').subscribe(() => {
            this.appIsOnline = false;
        });

        this.browserOnline = Observable.fromEvent(window, 'online').subscribe(() => {
            this.appIsOnline = true;
        });
        this.oEvents.subscribe('Login:success', () => {
            this.oNgZone.run(async () => {
                setTimeout(() => this.oIonicContent.resize(), 100);
                this.setEnvionmentBasedOnParameters();
                this.getRecords();
            });
        });
    }

    ionViewCanEnter() {
        if (
            /MSIE 10/i.test(navigator.userAgent) ||
            /MSIE 9/i.test(navigator.userAgent) ||
            /rv:11.0/i.test(navigator.userAgent)
        ) {
            // This is internet explorer 9 or 11
            const confirm = this.oAlertController.create({
                title: this.oAppSettings.IEStatementTitle,
                message: this.oAppSettings.IEStatement,
                buttons: [
                    {
                        text: 'OK',
                        handler: () => { }
                    }
                ]
            });
            confirm.present();
            return false;
        }
    }

    async getID(Name: string, ItemType: 'Table' | 'Report' | 'Table Group'): Promise<string> {
        let strGetWhat: WebApiGetWhatNames = 'table-group-id-by-name';
        if (ItemType == 'Table') strGetWhat = 'table-id-by-name';
        if (ItemType == 'Table Group') strGetWhat = 'table-group-id-by-name';
        if (ItemType == 'Report') strGetWhat = 'report-id-by-name';
        let oAPIReturn:
            | TableRecords
            | TableRecord
            | FieldRecords
            | FieldRecord
            | FieldTypes
            | IDValue
            | IDValues
            | AdminConfigRecord
            | AdminConfigRecords
            | DataTable
            | DataRow
            | LookupValues
            | ItemID
            | TableRightRecord
            | TableRightRecords
            | APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
                strGetWhat,
                0,
                0,
                '0',
                Name
            );
        if (
            oAPIReturn.kind === TableBuilderReturnKind.ErrorKind &&
            oAPIReturn.WebAPIMessage === 'The source contains no DataRows.'
        ) {
            this.booShowSpinner = false;
            return;
        } else if (
            oAPIReturn.kind === TableBuilderReturnKind.ErrorKind &&
            oAPIReturn.WebAPIMessageType === 'NotLoggedIn'
        ) {
            this.booShowSpinner = false;
            return;
        } else if (oAPIReturn.kind === TableBuilderReturnKind.ErrorKind) {
            if (oAPIReturn.WebAPIMessage === undefined) {
                oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
            }
            let toast = this.oToastController.create({
                message: oAPIReturn.WebAPIMessage,
                duration: 2000,
                position: 'middle'
            });
            toast.present();
            this.booShowSpinner = false;
            return;
        } else if (oAPIReturn.kind === TableBuilderReturnKind.ItemIDKind) {
            this.booShowSpinner = false;
            return oAPIReturn.ID.toString();
        } else {
            this.booShowSpinner = false;
            return '';
        }
    }

    async loadEmailTriggerTimes() {
        let oAPIReturn:
            | TableRecords
            | TableRecord
            | FieldRecords
            | FieldRecord
            | FieldTypes
            | IDValue
            | IDValues
            | AdminConfigRecord
            | AdminConfigRecords
            | DataTable
            | DataRow
            | LookupValues
            | ItemID
            | TableRightRecord
            | TableRightRecords
            | APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
                'email-trigger-times',
                0,
                0,
                '',
                ''
            );
        if (
            oAPIReturn.kind === TableBuilderReturnKind.ErrorKind &&
            oAPIReturn.WebAPIMessage === 'The source contains no DataRows.'
        ) {
            return;
        } else if (
            oAPIReturn.kind === TableBuilderReturnKind.ErrorKind &&
            oAPIReturn.WebAPIMessageType === 'NotLoggedIn'
        ) {
            return;
        } else if (oAPIReturn.kind === TableBuilderReturnKind.ErrorKind) {
            if (oAPIReturn.WebAPIMessage === undefined) {
                oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
            }
            let toast = this.oToastController.create({
                message: oAPIReturn.WebAPIMessage,
                duration: 2000,
                position: 'middle'
            });
            toast.present();
            this.booShowSpinner = false;
            return;
        } else if (oAPIReturn.kind === TableBuilderReturnKind.IDValuesKind) {
            this.oEmailTriggerTimes = oAPIReturn;
        }
    }

    async setEnvionmentBasedOnParameters() {
        if (
            this.oNavParams.get('strMode') ||
            this.oNavParams.get('strSelectedTableGroupID') ||
            this.oNavParams.get('strSelectedTableGroup') ||
            this.oNavParams.get('strID') ||
            this.oNavParams.get('strSelectedTable') ||
            this.oNavParams.get('ExecuteSQLID')
        ) {
            this.strMode = this.oNavParams.get('strMode');
            this.strSelectedTableGroupID = this.oNavParams.get('strSelectedTableGroupID');
            this.strSelectedTableGroup = this.oNavParams.get('strSelectedTableGroup');
            this.strID = this.oNavParams.get('strID');
            this.strSelectedTable = this.oNavParams.get('strSelectedTable');
            this.strExecuteSQLID = this.oNavParams.get('strExecuteSQLID');
        } else {
            if (this.oNavParams.get('url-action')) {
                let strURLAction: string = this.oNavParams.get('url-action');
                console.log('strURLAction' + strURLAction);
                debugger;
                if (strURLAction.startsWith('tg:')) {
                    this.strMode = 'Tables';
                    this.strSelectedTableGroup = strURLAction.substring(3);
                    console.log('calling getID with name: ' + this.strSelectedTableGroup);
                    this.strSelectedTableGroupID = await this.getID(this.strSelectedTableGroup, 'Table Group');
                } else if (strURLAction.startsWith('report:')) {
                    this.strMode = 'Execute SQL';
                    let strExecuteSQLName: string = strURLAction.substring(9);
                    this.strExecuteSQLID = await this.getID(strExecuteSQLName, 'Report');
                } else if (strURLAction.startsWith('reports:')) {
                    this.strMode = 'SQL';
                    this.strSelectedTable = strURLAction.substring(8);
                    this.strID = await this.getID(this.strSelectedTable, 'Table');
                } else {
                    this.strMode = 'Table Data';
                    this.strSelectedTable = strURLAction;
                    console.log('this.strSelectedTable' + this.strSelectedTable);
                    this.strID = await this.getID(this.strSelectedTable, 'Table');
                    console.log('this.strID' + this.strID);
                }
            }
        }

        let strRightsID: string = '';
        let strGetWhatRights: WebApiGetWhatNames = 'user-table-rights';
        if (this.strMode == 'Tables') {
            strRightsID = this.strSelectedTableGroupID;
            strGetWhatRights = 'user-table-group-rights';
        } else {
            strRightsID = this.strID;
            strGetWhatRights = 'user-table-rights';
        }

        let oAPIReturn:
            | TableRecords
            | TableRecord
            | FieldRecords
            | FieldRecord
            | FieldTypes
            | IDValue
            | IDValues
            | AdminConfigRecord
            | AdminConfigRecords
            | DataTable
            | DataRow
            | ItemID
            | LookupValues
            | TableRightRecords
            | TableRightRecord
            | APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
                strGetWhatRights,
                0,
                0,
                strRightsID,
                ''
            );
        if (
            oAPIReturn.kind === TableBuilderReturnKind.ErrorKind &&
            oAPIReturn.WebAPIMessage == 'The source contains no DataRows.'
        ) {
            this.oUserTablePermissions = null;
            this.booSystemAdminPermision = false;
            this.booEditorRightForTable = false;
            this.booDesignerRightForTable = false;
            this.booViewerRightForTable = false;
            this.booEditorRightForTableGroup = false;
            this.booDesignerRightForTableGroup = false;
            this.booViewerRightForTableGroup = false;
        } else if (oAPIReturn.kind === TableBuilderReturnKind.ErrorKind) {
            if (oAPIReturn.WebAPIMessage === undefined) {
                oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
            }
            let toast = this.oToastController.create({
                message: oAPIReturn.WebAPIMessage,
                duration: 2000,
                position: 'middle'
            });
            toast.present();
            this.oNavController.pop();
            this.booShowSpinner = false;
            return;
        } else if (oAPIReturn.kind === TableBuilderReturnKind.IDValuesKind) {
            this.oUserTablePermissions = oAPIReturn;
            this.booSystemAdminPermision = false;
            this.booEditorRightForTable = false;
            this.booDesignerRightForTable = false;
            this.booViewerRightForTable = false;
            this.booEditorRightForTableGroup = false;
            this.booDesignerRightForTableGroup = false;
            this.booViewerRightForTableGroup = false;
            for (let oPermission of this.oUserTablePermissions.Values) {
                if (oPermission.Value == 'system-admin') {
                    this.booSystemAdminPermision = true;
                } else if (oPermission.Value == 'table-right-editor') {
                    this.booEditorRightForTable = true;
                } else if (oPermission.Value == 'table-right-designer') {
                    this.booDesignerRightForTable = true;
                } else if (oPermission.Value == 'table-right-viewer') {
                    this.booViewerRightForTable = true;
                } else if (oPermission.Value == 'table-group-right-editor') {
                    this.booEditorRightForTableGroup = true;
                } else if (oPermission.Value == 'table-group-right-designer') {
                    this.booDesignerRightForTableGroup = true;
                } else if (oPermission.Value == 'table-group-right-viewer') {
                    this.booViewerRightForTableGroup = true;
                }
            }
        }
    }

    async ionViewDidEnter() {
        this.oUserProvider.checkLogin().then(async (booLoginOK) => {
            if (booLoginOK) {
                await this.setEnvionmentBasedOnParameters();
                this.intCurrentPage = 0;
                // this.strCurrentSearchString = '';
                this.getRecords();
                if (!this.oIntranetLinks) {
                    this.oUserProvider.getIntranetLinks().then((oLinks) => {
                        this.oIntranetLinks = oLinks;
                        this.oEvents.publish('InternetLinks:Recieved', this.oIntranetLinks);
                    });
                }
                this.loadEmailTriggerTimes();
            }
        });
    }

	/**
  Loads record for display based on intCurrentPage and strCurrentSearchString.
  */
    async getRecords() {
        this.booShowSpinner = true;
        let strWhatName: WebApiGetWhatNames = 'table-groups';
        let strID: string = '';

        if (this.strMode == 'Table Groups') {
            strID = '';
        } else if (this.strMode == 'Admin') {
            strID = '';
            if (!this.booSystemAdminPermision) {
                this.booShowSpinner = false;
                this.oNavController.pop();
                return;
            }
        } else if (this.strMode == 'Tables') {
            strID = this.strSelectedTableGroupID;
            if (!this.booViewerRightForTableGroup) {
                this.booShowSpinner = false;
                this.oNavController.pop();
                return;
            }
        } else if (this.strMode == 'Table Data') {
            strID = this.strID;
            if (!this.booViewerRightForTable || !this.booViewerRightForTableGroup) {
                this.booShowSpinner = false;
                this.oNavController.pop();
                return;
            }
        } else if (this.strMode == 'Fields') {
            strID = this.strID;
            if (!this.booDesignerRightForTable || !this.booDesignerRightForTableGroup) {
                this.oNavController.pop();
                this.booShowSpinner = false;
                return;
            }
        } else if (this.strMode == 'Workflow') {
            strID = this.strID;
            if (!this.booDesignerRightForTable || !this.booDesignerRightForTableGroup) {
                this.oNavController.pop();
                this.booShowSpinner = false;
                return;
            }
        } else if (this.strMode == 'SQL') {
            strID = this.strID;
            if (!this.booViewerRightForTable || !this.booViewerRightForTableGroup) {
                this.oNavController.pop();
                this.booShowSpinner = false;
                return;
            }
        }

        if (this.strMode == 'Tables') {
            strWhatName = 'tables-in-group';
        } else if (this.strMode == 'Execute SQL' && this.strExecuteSQLID != '') {
            strWhatName = 'execute-data-view';
            strID = this.strExecuteSQLID;
        } else if (this.strMode == 'Table Groups') {
            strWhatName = 'table-groups';
        } else if (this.strMode == 'Table Data') {
            strWhatName = 'table-data';
        } else if (this.strMode == 'Fields') {
            strWhatName = 'table-fields';
        } else if (this.strMode == 'Workflow') {
            strWhatName = 'table-workflow-steps';
        } else if (this.strMode == 'SQL') {
            strWhatName = 'data-views';
        } else if (this.strMode == 'Admin') {
            strWhatName = 'admin-config-records';
        } else {
            'table-groups';
        }
        let oAPIReturn:
            | TableRecords
            | TableRecord
            | FieldRecords
            | FieldRecord
            | WorkflowStepRecord
            | WorkflowStepRecords
            | FieldTypes
            | IDValue
            | IDValues
            | AdminConfigRecord
            | AdminConfigRecords
            | DataTable
            | DataRow
            | LookupValues
            | ItemID
            | TableRightRecord
            | TableRightRecords
            | APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
                strWhatName,
                this.intCurrentPage,
                this.intRecordsPerPage,
                strID,
                this.strCurrentSearchString
            );
        if (
            oAPIReturn.kind === TableBuilderReturnKind.ErrorKind &&
            oAPIReturn.WebAPIMessage === 'The source contains no DataRows.'
        ) {
            if (this.intCurrentPage === 0) {
                this.oRecords = null;
                this.oDataTable = null;
                this.oAdminConfigRecords = null;
            } else {
                this.booShowSpinner = false;
                return;
            }
        } else if (
            oAPIReturn.kind === TableBuilderReturnKind.ErrorKind &&
            oAPIReturn.WebAPIMessageType === 'NotLoggedIn'
        ) {
            this.booShowSpinner = false;
            return;
        } else if (oAPIReturn.kind === TableBuilderReturnKind.ErrorKind) {
            if (oAPIReturn.WebAPIMessage === undefined) {
                oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
            }
            let toast = this.oToastController.create({
                message: oAPIReturn.WebAPIMessage,
                duration: 2000,
                position: 'middle'
            });
            toast.present();
            this.booShowSpinner = false;
            return;
        } else if (oAPIReturn.kind === TableBuilderReturnKind.TableRecordsKind /*also groups*/) {
            if (this.intCurrentPage === 0) {
                this.oRecords = oAPIReturn.TableRecs;
                this.oDataTable = null;
                this.oAdminConfigRecords = null;
            } else {
                if (oAPIReturn.TableRecs.length > 0) {
                    this.oRecords = this.oRecords.concat(oAPIReturn.TableRecs);
                    this.oDataTable = null;
                    this.oAdminConfigRecords = null;
                }
            }
            if (this.oRecords && this.oRecords.length && this.oRecords.length > 0) {
                let oTableRecordsPinned: TableRecord[] = new Array();
                let oTableRecordsNotPinned: TableRecord[] = new Array();
                for (let oEachRecord of this.oRecords) {
                    if (oEachRecord.PinnedYN == 'Y') {
                        oTableRecordsPinned.push(oEachRecord);
                    } else {
                        oTableRecordsNotPinned.push(oEachRecord);
                    }
                }
                this.oRecords = oTableRecordsPinned;
                this.oRecords = this.oRecords.concat(oTableRecordsNotPinned);
            }
        } else if (oAPIReturn.kind === TableBuilderReturnKind.DataTableKind) {
            if (oAPIReturn.Values.length > 0) {
                this.oFieldRecords = Object.keys(oAPIReturn.Values[0]);
                if (this.oFieldRecords[0] == 'system______row_number_for_table_data') {
                    this.oFieldRecords.shift(); // remove system______row_number_for_table_data
                }
                if (this.oFieldRecords[0] == 'system______row_id_number_for_table_data') {
                    this.oFieldRecords.shift(); // remove system______row_id_number_for_table_data
                }
                if (this.oFieldRecords[0] == 'system______display_order_for_field') {
                    this.oFieldRecords.shift(); // remove
                }
                if (this.oFieldRecords[0] == 'system______id_number_for_field') {
                    this.oFieldRecords.shift(); // remove
                }
            }
            if (this.intCurrentPage == 0) {
                this.oRecords = null;
                this.oDataTable = <DataTable>{};
                this.oDataTable.Values = oAPIReturn.Values;
                this.oAdminConfigRecords = null;
            } else {
                if (oAPIReturn.Values.length > 0) {
                    this.oRecords = null;
                    this.oAdminConfigRecords = null;
                    this.oDataTable.Values = this.oDataTable.Values.concat(oAPIReturn.Values);
                }
            }
        } else if (oAPIReturn.kind === TableBuilderReturnKind.AdminConfigRecordsKind) {
            if (this.intCurrentPage === 0) {
                this.oAdminConfigRecords = oAPIReturn.Values;
                this.oRecords = null;
                this.oDataTable = null;
            } else {
                if (oAPIReturn.Values.length > 0) {
                    this.oAdminConfigRecords = this.oAdminConfigRecords.concat(oAPIReturn.Values);
                    this.oRecords = null;
                    this.oDataTable = null;
                }
            }
        }

        this.booShowSpinner = false;
    }

	/**
  Sets intCurrentPage and strCurrentSearchString and calls getRecords.
  */
    // async getSearchResults(ev: any) {
    async getSearchResults() {
        this.oNgZone.run(() => {
            // this.strCurrentSearchString = ev.target.value;
            this.intCurrentPage = 0;
            this.getRecords();
        });
    }

	/**
  Runs when the user scrolls to the bottow of the results.  Loads more records.
  */
    doInfinite(oInfiniteScroll) {
        this.intCurrentPage = this.intCurrentPage + 1;
        this.getRecords();
        oInfiniteScroll.complete();
    }

    editData(FieldID: string) {
        let strMode: EditScreenMode = 'Data';
        this.oNavController.push(EditPage, {
            id: FieldID,
            parentid: this.strID,
            mode: strMode
        });
    }

    executeSQLRow(id: string) {
        this.oNavController.push(ListPage, {
            strExecuteSQLID: id,
            strID: this.strID,
            strMode: 'Execute SQL'
        });
    }

    viewData(id: string, title: string) {
        let strMode: ListScreenMode = 'Table Data';
        this.oNavController.push(ListPage, {
            strID: id,
            strSelectedTable: title,
            strMode: strMode
        });
    }

    viewFieldsInTable(id: string, title: string) {
        let strMode: ListScreenMode = 'Fields';
        this.oNavController.push(ListPage, {
            strID: id,
            strSelectedTable: title,
            strSelectedTableGroupID: '',
            strSelectedTableGroup: '',
            strMode: strMode
        });
    }

    viewWorkflowInTable(id: string, title: string) {
        let strMode: ListScreenMode = 'Workflow';
        this.oNavController.push(ListPage, {
            strID: id,
            strSelectedTable: title,
            strSelectedTableGroupID: this.strSelectedTableGroupID,
            strSelectedTableGroup: this.strSelectedTableGroup,
            strMode: strMode
        });
    }


    editTableGroup(id: string) {
        let strMode: EditScreenMode = 'Table Group';
        this.oNavController.push(EditPage, {
            id: id,
            mode: strMode
        });
    }

    editTable(id: string) {
        let strMode: EditScreenMode = 'Table';
        let strParentID: string = '';
        if (id == '') {
            strParentID = this.strSelectedTableGroupID;
        }
        this.oNavController.push(EditPage, {
            id: id,
            parentid: strParentID,
            mode: strMode
        });
    }

    editField(id: string) {
        let strMode: EditScreenMode = 'Field';
        let strParentID: string = '';
        if (id == '') {
            strParentID = this.strID;
        }
        this.oNavController.push(EditPage, {
            id: id,
            parentid: strParentID,
            mode: strMode
        });
    }

    editAdminConfig(id: string) {
        this.strCurrentSearchString = '';
        let strMode: EditScreenMode = 'Admin';
        this.oNavController.push(EditPage, {
            id: id,
            mode: strMode
        });
    }

    async editDataView(id: string) {
        let strMode: EditScreenMode = 'SQL';
        if (id) {
            this.oNavController.push(EditPage, {
                id: id,
                mode: strMode
            });
        } else {
            this.oNavController.push(EditPage, {
                parentid: this.strID,
                mode: strMode
            });
        }
    }

    viewTablesInGroup(id: string, title: string) {
        let strMode: ListScreenMode = 'Tables';
        this.oNavController.push(ListPage, {
            strID: '',
            strSelectedTable: '',
            strSelectedTableGroupID: id,
            strSelectedTableGroup: title,
            strMode: strMode
        });
    }

    viewTableGroups() {
        let strMode: ListScreenMode = 'Table Groups';
        this.oNavController.push(ListPage, {
            strID: '',
            strSelectedTable: '',
            strSelectedTableGroupID: '',
            strSelectedTableGroup: '',
            strMode: strMode
        });
    }

    viewConfigRecords() {
        let strMode: ListScreenMode = 'Admin';
        this.oNavController.push(ListPage, {
            strID: '',
            strSelectedTable: '',
            strSelectedTableGroupID: '',
            strSelectedTableGroup: '',
            strMode: strMode
        });
    }

    viewDataViewsInTable(id: string, title: string) {
        let strMode: ListScreenMode = 'SQL';
        this.oNavController.push(ListPage, {
            strID: id,
            strSelectedTable: title,
            strMode: strMode
        });
    }

    async moveFieldUpList(index: number) {
        if (this.oDataTable.Values.length < 2) return;
        if (!this.oDataTable.Values[index - 1]) return;
        const strThisID: string = this.oDataTable.Values[index].system______id_number_for_field;
        const strOtherID: string = this.oDataTable.Values[index - 1].system______id_number_for_field;
        const oAPIReturn:
            | APISuccess
            | APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.updateData(
                'field',
                'swap-order',
                strThisID,
                strOtherID,
                this.oUserProvider.oUserInfo.UserName,
                this.oUserProvider.oUserInfo.EmployeeID
            );

        if (oAPIReturn.kind === TableBuilderReturnKind.SuccessKind) {
            this.getRecords();
            return;
        } else if (oAPIReturn.WebAPIMessageType === 'NotLoggedIn') {
            return;
        } else if (oAPIReturn.WebAPIMessageType === 'GetAuthyCode') {
            let alert = this.oAlertController.create({
                title: this.oAppSettings.AuthyOneTouchStatement,
                buttons: [
                    {
                        text: 'OK',
                        handler: () => {
                            this.moveFieldUpList(index);
                            return;
                        }
                    }
                ]
            });
            alert.present();
        } else {
            if (oAPIReturn.WebAPIMessage === undefined) {
                oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
            }
            let toast = this.oToastController.create({
                message: oAPIReturn.WebAPIMessage,
                duration: 2000,
                position: 'middle'
            });
            toast.present();
        }
    }

    async moveFieldDownList(index: number) {
        if (this.oDataTable.Values.length < 2) return;
        if (!this.oDataTable.Values[index + 1]) return;
        const strThisID: string = this.oDataTable.Values[index].system______id_number_for_field;
        const strOtherID: string = this.oDataTable.Values[index + 1].system______id_number_for_field;
        const oAPIReturn:
            | APISuccess
            | APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.updateData(
                'field',
                'swap-order',
                strThisID,
                strOtherID,
                this.oUserProvider.oUserInfo.UserName,
                this.oUserProvider.oUserInfo.EmployeeID
            );

        if (oAPIReturn.kind === TableBuilderReturnKind.SuccessKind) {
            this.getRecords();
            return;
        } else if (oAPIReturn.WebAPIMessageType === 'NotLoggedIn') {
            return;
        } else if (oAPIReturn.WebAPIMessageType === 'GetAuthyCode') {
            let alert = this.oAlertController.create({
                title: this.oAppSettings.AuthyOneTouchStatement,
                buttons: [
                    {
                        text: 'OK',
                        handler: () => {
                            this.moveFieldUpList(index);
                            return;
                        }
                    }
                ]
            });
            alert.present();
        } else {
            if (oAPIReturn.WebAPIMessage === undefined) {
                oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
            }
            let toast = this.oToastController.create({
                message: oAPIReturn.WebAPIMessage,
                duration: 2000,
                position: 'middle'
            });
            toast.present();
        }
    }

    async moveFieldToTopOfList(index: number) {
        if (this.oDataTable.Values.length < 2) return;
        if (index == 0) return;
        if (!this.oDataTable.Values[0]) return;
        const strThisID: string = this.oDataTable.Values[index].system______id_number_for_field;
        const strOtherID: string = this.oDataTable.Values[0].system______id_number_for_field;
        const oAPIReturn:
            | APISuccess
            | APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.updateData(
                'field',
                'swap-order',
                strThisID,
                strOtherID,
                this.oUserProvider.oUserInfo.UserName,
                this.oUserProvider.oUserInfo.EmployeeID
            );

        if (oAPIReturn.kind === TableBuilderReturnKind.SuccessKind) {
            this.getRecords();
            return;
        } else if (oAPIReturn.WebAPIMessageType === 'NotLoggedIn') {
            return;
        } else if (oAPIReturn.WebAPIMessageType === 'GetAuthyCode') {
            let alert = this.oAlertController.create({
                title: this.oAppSettings.AuthyOneTouchStatement,
                buttons: [
                    {
                        text: 'OK',
                        handler: () => {
                            this.moveFieldUpList(index);
                            return;
                        }
                    }
                ]
            });
            alert.present();
        } else {
            if (oAPIReturn.WebAPIMessage === undefined) {
                oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
            }
            let toast = this.oToastController.create({
                message: oAPIReturn.WebAPIMessage,
                duration: 2000,
                position: 'middle'
            });
            toast.present();
        }
    }

    async moveFieldToBottomOfList(index: number) {
        if (this.oDataTable.Values.length < 2) return;
        if (index == this.oDataTable.Values.length - 1) return;
        if (!this.oDataTable.Values[this.oDataTable.Values.length - 1]) return;
        const strThisID: string = this.oDataTable.Values[index].system______id_number_for_field;
        const strOtherID: string = this.oDataTable.Values[this.oDataTable.Values.length - 1]
            .system______id_number_for_field;
        const oAPIReturn:
            | APISuccess
            | APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.updateData(
                'field',
                'swap-order',
                strThisID,
                strOtherID,
                this.oUserProvider.oUserInfo.UserName,
                this.oUserProvider.oUserInfo.EmployeeID
            );

        if (oAPIReturn.kind === TableBuilderReturnKind.SuccessKind) {
            this.getRecords();
            return;
        } else if (oAPIReturn.WebAPIMessageType === 'NotLoggedIn') {
            return;
        } else if (oAPIReturn.WebAPIMessageType === 'GetAuthyCode') {
            let alert = this.oAlertController.create({
                title: this.oAppSettings.AuthyOneTouchStatement,
                buttons: [
                    {
                        text: 'OK',
                        handler: () => {
                            this.moveFieldUpList(index);
                            return;
                        }
                    }
                ]
            });
            alert.present();
        } else {
            if (oAPIReturn.WebAPIMessage === undefined) {
                oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
            }
            let toast = this.oToastController.create({
                message: oAPIReturn.WebAPIMessage,
                duration: 2000,
                position: 'middle'
            });
            toast.present();
        }
    }

    async getEmailTriggerSubsriptionTimesForRow(rowid: number): Promise<IDValue> {
        let oAPIReturn:
            | TableRecords
            | TableRecord
            | FieldRecords
            | FieldRecord
            | FieldTypes
            | IDValue
            | IDValues
            | AdminConfigRecord
            | AdminConfigRecords
            | DataTable
            | DataRow
            | ItemID
            | LookupValues
            | TableRightRecord
            | TableRightRecords
            | APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.getData(
                'check-email-trigger-subscription',
                0,
                0,
                rowid.toString(),
                this.oUserProvider.oUserInfo.UserName
            );
        if (
            oAPIReturn.kind === TableBuilderReturnKind.ErrorKind &&
            oAPIReturn.WebAPIMessage === 'The source contains no DataRows.'
        ) {
            return;
        } else if (
            oAPIReturn.kind === TableBuilderReturnKind.ErrorKind &&
            oAPIReturn.WebAPIMessageType === 'NotLoggedIn'
        ) {
            return;
        } else if (oAPIReturn.kind === TableBuilderReturnKind.ErrorKind) {
            if (oAPIReturn.WebAPIMessage === undefined) {
                oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
            }
            let toast = this.oToastController.create({
                message: oAPIReturn.WebAPIMessage,
                duration: 2000,
                position: 'middle'
            });
            toast.present();
            this.booShowSpinner = false;
            return;
        } else if (oAPIReturn.kind === TableBuilderReturnKind.IDValueKind) {
            return oAPIReturn;
        }
    }

    async clickedSQLReportSubscription(rowid: number) {
        let oCurrentSubsciption: IDValue = await this.getEmailTriggerSubsriptionTimesForRow(rowid);
        let strCurrentSubsciptionTimeID: string = '';
        if (oCurrentSubsciption && oCurrentSubsciption.Value) {
            strCurrentSubsciptionTimeID = oCurrentSubsciption.Value;
        }

        let radioAlert = this.oAlertController.create();
        radioAlert.setTitle('Email Subsciption for this SQL Report');
        let booChecked: boolean = false;
        for (let oTimes of this.oEmailTriggerTimes.Values) {
            if (strCurrentSubsciptionTimeID === '') {
                if (oTimes.ID == '516' /*516=None*/) {
                    booChecked = true;
                } else {
                    booChecked = false;
                }
            } else {
                if (oTimes.ID == strCurrentSubsciptionTimeID) {
                    booChecked = true;
                } else {
                    booChecked = false;
                }
            }
            radioAlert.addInput({
                type: 'radio',
                label: oTimes.Value,
                value: oTimes.ID,
                checked: booChecked
            });
        }

        radioAlert.addButton('Cancel');
        radioAlert.addButton({
            text: 'OK',
            handler: (data: string) => {
                if (data != this.strCurrentSearchString) {
                    // subscribe: sql-report-subscription, insert: id=rowid, value=time id
                    this.setEmailTriggerSubsriptionTimesForRow(rowid, data);
                }
            }
        });
        radioAlert.present();
    }

    async setEmailTriggerSubsriptionTimesForRow(rowid: number, EmailTimeID: string) {
        const oAPIReturn:
            | APISuccess
            | APIError = await this.oTableBuilderProvider.oTableBuilderWebApiProvider.updateData(
                'sql-report-subscription',
                'insert',
                rowid.toString(),
                EmailTimeID,
                this.oUserProvider.oUserInfo.UserName,
                this.oUserProvider.oUserInfo.EmployeeID
            );

        if (oAPIReturn.kind === TableBuilderReturnKind.SuccessKind) {
            return;
        } else if (oAPIReturn.WebAPIMessageType === 'NotLoggedIn') {
            return;
        } else if (oAPIReturn.WebAPIMessageType === 'GetAuthyCode') {
            let alert = this.oAlertController.create({
                title: this.oAppSettings.AuthyOneTouchStatement,
                buttons: [
                    {
                        text: 'OK',
                        handler: () => {
                            this.setEmailTriggerSubsriptionTimesForRow(rowid, EmailTimeID);
                            return;
                        }
                    }
                ]
            });
            alert.present();
        } else {
            if (oAPIReturn.WebAPIMessage === undefined) {
                oAPIReturn.WebAPIMessage = 'Error: No connection to server.';
            }
            let toast = this.oToastController.create({
                message: oAPIReturn.WebAPIMessage,
                duration: 2000,
                position: 'middle'
            });
            toast.present();
        }
    }
}
