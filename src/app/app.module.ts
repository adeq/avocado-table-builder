import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MyApp } from './app.component';
import { EditPageModule } from '../pages/edit/edit.module';
import { ListPageModule } from '../pages/list/list.module';
import { LookupPageModule } from '../pages/lookup/lookup.module';

import { IonicStorageModule } from '@ionic/storage';
import { TableBuilderProvider } from '../providers/table-builder/table-builder';
import { TableBuilderWebApiProvider } from '../providers/table-builder-web-api/table-builder-web-api';
import { UserProvider } from '../providers/user/user';
import { AppSettings } from '../providers/app-settings/app-settings';
import { CodemirrorModule } from '@nomadreservations/ngx-codemirror';
import { QuillModule } from 'ngx-quill-wrapper';
import { QUILL_CONFIG } from 'ngx-quill-wrapper';
import { QuillConfigInterface } from 'ngx-quill-wrapper';

// https://quilljs.com/docs/modules/toolbar/
const toolbarOptions = [
	['bold', 'italic', 'underline', 'strike'], // toggled buttons
	['blockquote', 'code-block'],

	[{ list: 'ordered' }, { list: 'bullet' }],
	[{ script: 'sub' }, { script: 'super' }], // superscript/subscript
	[{ indent: '-1' }, { indent: '+1' }], // outdent/indent

	[{ header: [1, 2, 3, 4, 5, 6, false] }],

	[{ align: [] }],
	['link'],

	['clean'] // remove formatting button
];
const DEFAULT_QUILL_CONFIG: QuillConfigInterface = {
	theme: 'snow',
	modules: {
		toolbar: toolbarOptions
	},
	placeholder: 'Editor'
};

@NgModule({
	declarations: [MyApp],
	imports: [
		BrowserAnimationsModule,
		BrowserModule,
		HttpModule,
		IonicModule.forRoot(MyApp, { navExitApp: false, platforms: { core: { mode: 'wp' } } }),
		IonicStorageModule.forRoot(),
		QuillModule,
		EditPageModule,
		ListPageModule,
		LookupPageModule,
		CodemirrorModule
	],
	bootstrap: [IonicApp],
	entryComponents: [MyApp],
	providers: [
		// StatusBar,
		// SplashScreen,
		{ provide: ErrorHandler, useClass: IonicErrorHandler },
		{
			provide: QUILL_CONFIG,
			useValue: DEFAULT_QUILL_CONFIG
		},
		TableBuilderProvider,
		TableBuilderWebApiProvider,
		UserProvider,
		AppSettings
	]
})
export class AppModule { }
