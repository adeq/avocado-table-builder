import { Component, ViewChild } from '@angular/core';
import { AlertController, Nav, Platform, ToastController } from 'ionic-angular';
// import { StatusBar } from '@ionic-native/status-bar';
// import { SplashScreen } from '@ionic-native/splash-screen';

import { Storage } from '@ionic/storage';
import { ListPage } from '../pages/list/list';
// import { LoginPage } from '../pages/login/login';
import { UserProvider } from '../providers/user/user';
import { Events } from 'ionic-angular';
import { AppSettings } from '../providers/app-settings/app-settings';

export interface IntranetLinks {
	title: string;
	URL: string;
}

@Component({
	templateUrl: 'app.html'
})
export class MyApp {
	@ViewChild(Nav) nav: Nav;

	rootPage: any = ListPage;
	booIsApp: boolean = false;
	oIntranetLinks: IntranetLinks[];

	constructor(
		public oPlatform: Platform,
		// public oStatusBar: StatusBar,
		// public oSplashScreen: SplashScreen,
		private oAlertController: AlertController,
		public oStorage: Storage,
		public oUserProvider: UserProvider,
		private oEvents: Events,
		private oToastController: ToastController,
		private oAppSettings: AppSettings
	) {
		this.initializeApp();
	}

	initializeApp() {
		this.oPlatform.ready().then(() => {
			// Okay, so the platform is ready and our plugins are available.
			// Here you can do any higher level native things you might need.
			// this.oStatusBar.styleDefault();
			// this.oSplashScreen.hide();

			if (this.oPlatform.is('core') || this.oPlatform.is('mobileweb')) {
				// we are running in a browser or mobile web browser
				this.booIsApp = false;

				this.oEvents.subscribe('InternetLinks:Recieved', (oLinks: IntranetLinks[]) => {
					this.oIntranetLinks = oLinks;
				});
			} else {
				this.booIsApp = true;
			}

			const isIos = () => {
				const userAgent = window.navigator.userAgent.toLowerCase();
				return /iphone|ipad|ipod/.test(userAgent);
			};

			const isInStandaloneMode = () => 'standalone' in window.navigator && window.navigator['standalone'];

			if (isIos() && !isInStandaloneMode()) {
				let toast = this.oToastController.create({
					message:
						'Install this webapp on your iPhone: Tap the share icon below and then select "Add to Home Screen"',
					position: 'bottom',
					duration: 10000,
					showCloseButton: true
				});
				toast.present();
			}
		});
	}

	exportSpreadsheet() {
		this.oEvents.publish('ExportSpreadsheet:clicked');
	}

	ngOnInit() {
		console.log('Cheking for PWA update...');
		if (window['isUpdateAvailable']) {
			window['isUpdateAvailable'].then((isAvailable) => {
				console.log('PWA update available: ', isAvailable);
				if (isAvailable) {
					const toast = this.oToastController.create({
						message: 'New Version available! Reloading the App automatically in 5 seconds...',
						position: 'bottom',
						showCloseButton: true,
						duration: 5000
					});
					toast.onWillDismiss(() => {
						window.location.reload();
					});
					toast.present();
				}
			});
		}
	}

	async logout() {
		await this.oStorage.set('id_token', '');
		this.nav.setRoot(ListPage);
	}

	async showAbout() {
		let alert = this.oAlertController.create({
			title: 'About',
			subTitle: '<b>Avocado Table Builder</b> v' + this.oAppSettings.Version,
			buttons: [ 'Dismiss' ]
		});
		alert.present();
	}

	openPage(page) {
		// Reset the content nav to have just this page
		// we wouldn't want the back button to show in this scenario
		this.nav.setRoot(page.component);
	}

	openLink(strLink: string) {
		window.open(strLink, '_self', 'location=yes');
	}
}
